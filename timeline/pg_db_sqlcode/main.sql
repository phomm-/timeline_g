﻿-- DROP DATABASE "Timeline"

CREATE DATABASE "Timeline"
  WITH ENCODING='UTF8'
       CONNECTION LIMIT=-1;

---------------------------------------------------

CREATE TABLE timeparts
(
  id serial NOT NULL,
  name character varying(50) NOT NULL,
  title character varying(50) NOT NULL,
  minval integer,
  maxval integer,
  CONSTRAINT pk_timeparts PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE timeparts
  OWNER TO postgres;
  
CREATE TABLE timelines
(
  id serial NOT NULL,
  id_timepart integer NULL,  
  name character varying(50) NOT NULL UNIQUE,
  memo text NULL,
  datestart date NULL,
  dateend date NULL,
  len integer NOT NULL DEFAULT 1000,
  Y integer NOT NULL DEFAULT 500,
  color integer NOT NULL DEFAULT 0,
  intervalvalue integer,
  CONSTRAINT pk_timelines PRIMARY KEY (id),
  CONSTRAINT fk_id_timepart FOREIGN KEY (id_timepart)
    REFERENCES timeparts (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE timelines
  OWNER TO postgres;
  
  
CREATE TABLE storylines
(
  id serial NOT NULL,
  id_timeline integer NULL,
  name character varying(50) NOT NULL,
  fullname character varying(500) NULL,
  memo text NULL,
  notes text NULL,
  storylinetype character varying(50) NULL,
  len integer NOT NULL DEFAULT 1000,
  color integer NOT NULL DEFAULT 0,
  vis_top integer NOT NULL DEFAULT 0,
  vis_height integer NOT NULL DEFAULT 50,
  line_width integer NOT NULL DEFAULT 1,
  CONSTRAINT pk_storylines PRIMARY KEY (id),  
  CONSTRAINT fk_id_timeline FOREIGN KEY (id_timeline)
    REFERENCES timelines (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE storylines
  OWNER TO postgres;
  
    
CREATE TABLE events
(
  id serial NOT NULL,
  id_timeline integer NULL,
  id_storyline integer NULL,
  name character varying(50) NOT NULL,
  fullname character varying(500) NULL,
  memo text NULL,
  notes text NULL,
  datestart date NULL,
  dateend date NULL,
  color integer NOT NULL DEFAULT 0,
  vis_top integer NOT NULL DEFAULT 0,
  vis_height integer NOT NULL DEFAULT 50,  
  vis_left integer NOT NULL DEFAULT 0,
  vis_width integer NOT NULL DEFAULT 150,
  shape integer NOT NULL DEFAULT 0,
  line_width integer NOT NULL DEFAULT 1,
  CONSTRAINT pk_events PRIMARY KEY (id),
  CONSTRAINT fk_id_timeline FOREIGN KEY (id_timeline)
    REFERENCES timelines (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_id_storyline FOREIGN KEY (id_storyline)
    REFERENCES storylines (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE events
  OWNER TO postgres;  
  
  
CREATE TABLE eventlinks
(
  id serial NOT NULL,
  id_eventfrom integer NOT NULL,
  id_eventto integer NOT NULL,
  color integer NOT NULL DEFAULT 0,
  line_width integer NOT NULL DEFAULT 1,
  CONSTRAINT pk_eventlinks PRIMARY KEY (id),
  CONSTRAINT fk_id_eventfrom FOREIGN KEY (id_eventfrom)
    REFERENCES events (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_id_eventto FOREIGN KEY (id_eventto)
    REFERENCES events (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION    
)
WITH (
  OIDS=FALSE
);
ALTER TABLE eventlinks
  OWNER TO postgres;
   
   
  
CREATE TABLE areas
(
  id serial NOT NULL,
  name character varying(50) NOT NULL,
  country character varying(50) NULL,
  city character varying(50) NULL,
  place character varying(500) NULL,
  notes text NULL,
  memo text NULL,
  CONSTRAINT pk_areas PRIMARY KEY (id)  
)
WITH (
  OIDS=FALSE
);
ALTER TABLE areas
  OWNER TO postgres; 
  
CREATE TABLE persons
(
  id serial NOT NULL,
  name character varying(50) NOT NULL,
  lastname character varying(50) NULL,
  firstname character varying(50) NULL,
  middlename character varying(50) NULL,
  persontype character varying(50) NULL,
  gender character varying(10) NULL,
  birth date NULL,
  death date NULL,
  notes text,
  memo text,
  CONSTRAINT pk_persons PRIMARY KEY (id)  
)
WITH (
  OIDS=FALSE
);
ALTER TABLE persons
  OWNER TO postgres;
  
   
CREATE TABLE eventpersons
(
  id serial NOT NULL,
  id_event integer NOT NULL,
  id_person integer NOT NULL,
  personrole character varying(50) NULL,
  CONSTRAINT pk_eventpersons PRIMARY KEY (id),
  CONSTRAINT fk_id_event FOREIGN KEY (id_event)
    REFERENCES events (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_id_person FOREIGN KEY (id_person)
    REFERENCES persons (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION  
)
WITH (
  OIDS=FALSE
);
ALTER TABLE eventpersons
  OWNER TO postgres;  
  
  
CREATE TABLE eventareas
(
  id serial NOT NULL,
  id_event integer NOT NULL,
  id_area integer NOT NULL,
  CONSTRAINT pk_eventareas PRIMARY KEY (id),
  CONSTRAINT fk_id_event FOREIGN KEY (id_event)
    REFERENCES events (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_id_area FOREIGN KEY (id_area)
    REFERENCES persons (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION  
)
WITH (
  OIDS=FALSE
);
ALTER TABLE eventareas
  OWNER TO postgres;
  
       
CREATE TABLE images
(
  id serial NOT NULL,
  id_area integer NULL,
  id_event integer NULL,
  id_person integer NULL,
  name character varying(50),
  memo text,
  picture bytea,
  CONSTRAINT pk_images PRIMARY KEY (id),
  CONSTRAINT fk_id_event FOREIGN KEY (id_event)
      REFERENCES events (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_id_person FOREIGN KEY (id_person)
      REFERENCES persons (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_id_area FOREIGN KEY (id_area)
      REFERENCES areas (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);
ALTER TABLE images
  OWNER TO postgres;
  
-----------------------------------------------------------  
  
INSERT INTO timeparts(id, name, title, minval, maxval)
VALUES (0, 'секунды', 'сек.', 1, 60);         
INSERT INTO timeparts(id, name, title, minval, maxval)
VALUES (1, 'минуты', 'мин.', 1, 60);
INSERT INTO timeparts(id, name, title, minval, maxval)
VALUES (2, 'часы', 'ч.', 1, 24);
INSERT INTO timeparts(id, name, title, minval, maxval)
VALUES (3, 'дни', 'дн.', 1, 31);
INSERT INTO timeparts(id, name, title, minval, maxval)
VALUES (4, 'недели', 'нед.', 0, 53);
INSERT INTO timeparts(id, name, title, minval, maxval)
VALUES (5, 'месяцы', 'мес.', 1, 12);
INSERT INTO timeparts(id, name, title, minval, maxval)
VALUES (6, 'годы', 'лет', 0, 5000);
INSERT INTO timeparts(id, name, title, minval, maxval)
VALUES (7, 'века', 'век', 0, 50);
INSERT INTO timeparts(id, name, title, minval, maxval)
VALUES (8, 'тысячелетия', 'т. л.', 0, 0);