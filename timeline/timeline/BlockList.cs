﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;


namespace Timeline
{
    class BlockList : List<BlockObject>
    {
        public new void Add(BlockObject block, bool isnew = true)
        {
            base.Add(block);
            if (isnew)
                TimeLine.Line.RegBlock(block);
        }
        public bool rightNowBuildLink { get; private set; }
        public bool rightNowSelect { get; set; }
        // ---- для движения блока
        public bool moveBlock { get; private set; }
        public bool resizeBlock { get; private set; }
        public bool droppingBlock { get; private set; }
        public BlockObject movableBlock { get; private set; }
        public Size mvBlockPointDelta { get; private set; }
        // ----
        // ---- для создания связи
        public BlocksLink curLink { get; private set; }
        // ----
        public BlockObject curBlock { get; set; }

        public Action<BlockObject> LastChanged;

        public BlockObject Last()
        {
            return this.LastOrDefault(x => x.Selected && !x.IsTimeLine);
        }

        /// <summary>
        /// процесс - получение блока, над которым сейчас курсор
        /// </summary>
        public BlockObject getCurrentBlockObject(Point point, bool skipLines = false)
        {
            BlockObject ourBlock = null;
            BlockObject[] curbl = this.ToArray();
            int curblLen = curbl.Length;
            for (int i = 0; i < curblLen; i++)
            {

                if (curbl[(curblLen - 1) - i].isInside(point))
                {
                    ourBlock = curbl[(curblLen - 1) - i];
                    if (!(skipLines && curbl[(curblLen - 1) - i].IsLine))
                        break;
                }
                //else if (curbl[(curblLen - 1) - i].isOnStigmates(point))
                //{
                //    ourBlock = curbl[(curblLen - 1) - i];
                //    break;
                //}
            }
            return ourBlock;
        }

        public bool StartMove(Point PreviousPoint)
        {
            BlockObject ourBlock = getCurrentBlockObject(PreviousPoint, true);
            if (ourBlock != null)
            {
                movableBlock = ourBlock;
                if (ourBlock.isInsideResArea(PreviousPoint))
                {
                    resizeBlock = true;
                    mvBlockPointDelta = ourBlock.Size;
                }
                else
                {
                    moveBlock = true;
                    mvBlockPointDelta = new Size((PreviousPoint.X - ourBlock.X), (PreviousPoint.Y - ourBlock.Y));
                }
                this.Remove(ourBlock);
                return true;
            }
            return false;
        }


        public string StartLink(Point PreviousPoint, Pen linkPen)
        {
            string blockAnswer = "";
            // создаем связь и записываем в нее начальный блок, если конечно нажатие было на блоке
            BlockObject ourBlock = getCurrentBlockObject(PreviousPoint);
            if (ourBlock != null)
            {
                blockAnswer = ourBlock.CanAddLink(true);
                if (blockAnswer == "")
                {
                    curLink = new BlocksLink(ourBlock, linkPen);
                    rightNowBuildLink = true;
                }
            }
            return blockAnswer;
        }

        public string EndLink(Point place, Graphics g, PictureBox Zone)
        {
            string blockAnswer = "";
            rightNowBuildLink = false;
            BlockObject ourBlock = getCurrentBlockObject(place);
            if (ourBlock == null)
                return blockAnswer;
            // можно ли создать связь
            blockAnswer = ourBlock.CanAddLink(false);

            if (blockAnswer == "")
            {
                if (ourBlock != null)
                {
                    curLink.Second = ourBlock;
                    curLink.FindOptimalPoints();
                    curLink.DrawIt(g, Zone);
                    curLink.Register(true);
                }
            }
            else if (blockAnswer != null)
            {
                curLink = null;
            }
            return blockAnswer;
        }

        public bool StartSelection(Point PreviousPoint)
        {
            BlockObject ourBlock = getCurrentBlockObject(PreviousPoint);
            if (ourBlock != null)
            {
                ourBlock.Selected = !ourBlock.Selected;
                EndSelection();
                return true;
            }
            else { rightNowSelect = true; }
            return false;
        }

        public void EndSelection()
        {
            if (LastChanged != null)
                LastChanged(Last());
        }

        public int Amount(bool onlySelected)
        {
            int cnt = this.Count;
            if (!onlySelected)
            {
                if (moveBlock || resizeBlock) { cnt++; }
                return cnt;
            }
            cnt = 0;
            foreach (BlockObject one in this)
                if (one.Selected)
                    cnt++;
            if (moveBlock || resizeBlock)
                if (movableBlock.Selected)
                    cnt++;
            return cnt;
        }

        public bool EditReady(Point place)
        {
            BlockObject ourBlock = getCurrentBlockObject(place);
            if (ourBlock != null)
            {
                curBlock = ourBlock;
                return true;
            }
            return false;
        }

        public void Drop(Point place)
        {
            if (droppingBlock) // бросание блока в сюжлинию
            {
                BlockObject lineBlock = getCurrentBlockObject(place);
                lineBlock.AddChild(movableBlock);
            }
            if (moveBlock || resizeBlock) // только что передвинули какую-то фигуру или изменили её размер
            {
                moveBlock = false;
                resizeBlock = false;
                movableBlock.CompleteCorrection();
                if (!droppingBlock && movableBlock.IsAtLine)
                    movableBlock.RemoveMeFromLine();
            }
            droppingBlock = false;
            this.Add(movableBlock, false);
        }

        public void Move(Point place, Graphics g, PictureBox Zone, Action repaint, Cursor cursor)
        {
            movableBlock.PositionCorrection(place, mvBlockPointDelta);
            repaint();
            movableBlock.DrawIt(g, Zone, true);
            droppingBlock = false;
            if (!movableBlock.IsLine)
            {
                BlockObject ourBlock = getCurrentBlockObject(place);
                if (ourBlock != null && ourBlock.IsLine)
                {
                    cursor = Cursors.Hand;
                    droppingBlock = true;
                }
                else
                    cursor = Cursors.Default;
            }
        }

        public void Resize(Point place, Point prev, Graphics g, PictureBox Zone, Action repaint)
        {
            movableBlock.SizeCorrection(place, prev, mvBlockPointDelta);
            repaint();
            movableBlock.DrawIt(g, Zone, true);
        }


        internal void ClearAllBlocks()
        {
            for (int i = Count - 1; i != -1; i--)
                if (!this[i].IsTimeLine)
                    Remove(this[i]);
        }

        public void RemoveBlock(BlockObject block)
        {
            foreach (BlockObject one in this)
                if (one.parent == block)
                    one.parent = null;
            Remove(block);
        }
    }
}
