﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Opulos.Core.UI;
using System.Linq;

namespace Timeline
{
    public partial class Form1 : Form
    {
        #region ' объявление переменных'

        TableLayoutPanel tlpTimeline;
        TableLayoutPanel tlpEvent;
        TableLayoutPanel tlpStoryline;
        TableLayoutPanel tlpPerson;
        TableLayoutPanel tlpArea;
        TableLayoutPanel tlpImage;
        TextBox edtEventName;
        TextBox edtEventMemo;
        TextBox edtTimeLineName;
        TextBox edtTimeLineMemo;
        TextBox edtTimeLineDateStart;
        TextBox edtTimeLineDateEnd;
        NumericUpDown nudTimeLineLen;
        NumericUpDown nudTimeLineInterval;
        ComboBox cbTimeLineTimePart;

        TextBox edtEventDateStart;
        TextBox edtEventDateEnd;

        ListView lstPersons;
        ListView lstAreas;
        ListView lstImages;

        Button btnManagePerson;
        Button btnManageArea;
        Button btnManageImage;

        Button btnFitEventToDate;

        TextBox edtStoryLineName;
        TextBox edtStoryLineMemo;
        TextBox edtStoryLineFullName;
        TextBox edtStoryLineType;
        TextBox edtStoryLineNotes;

        Point PreviousPoint;
        Bitmap bmp = null;
        Graphics g;
        int PenSize = 1;

        MouseMode mode = MouseMode.Select;


        /// <summary>
        /// список блоков на сцене
        /// </summary>
        BlockList blocks { get { return TimeLine.Line == null ? null : TimeLine.blocks; } }


        Size zoneSize;
        /// <summary>
        /// регулирование размеров рабочей области
        /// </summary>
        Size ZoneSize
        {
            get { return zoneSize; }
            set
            {
                zoneSize = value;
                if (Zone.Image != null)
                {
                    bmp = new Bitmap(Zone.Image);
                    Zone.Image.Dispose();
                }
                else
                    bmp = new Bitmap(ZoneSize.Width, ZoneSize.Height);

                Zone.Size = ZoneSize;
                Zone.Image = new Bitmap(ZoneSize.Width, ZoneSize.Height);
                g = Graphics.FromImage(Zone.Image);
                g.FillRectangle(new SolidBrush(Color.White), 0, 0, ZoneSize.Width, ZoneSize.Height);
                g.DrawImage(bmp, 0, 0);
                g.Dispose();
                bool doRepaint = false;
                if (blocks != null)
                    foreach (BlockObject one in blocks)
                    {
                        if (one.IsLine)
                        {
                            one.SetWidth(ZoneSize.Width);
                            doRepaint = true;
                        }
                        if (one.IsTimeLine)
                        {
                            one.SetWidth(ZoneSize.Width);
                            one.SetY(Zone.Height - Consts.TimelineHeight);
                            doRepaint = true;
                        }
                    }
                if (doRepaint)
                    repaint();
            }
        }

        #endregion

        private void TimeLineChanged(bool doshow)
        {
            Zone.Visible = doshow;
            tlpTimeline.Enabled = doshow;
            if (doshow)
            {
                edtTimeLineDateStart.TextChanged -= TimeLineReSection;
                edtTimeLineDateEnd.TextChanged -= TimeLineReSection;
                nudTimeLineLen.ValueChanged -= TimeLineReSection;
                nudTimeLineInterval.ValueChanged -= TimeLineReSection;
                cbTimeLineTimePart.SelectedIndexChanged -= TimeLineReSection;
                edtTimeLineName.Text = TimeLine.Line.Name;
                edtTimeLineMemo.Text = TimeLine.Line.Memo;
                edtTimeLineDateStart.Text = TimeLine.Line.DateStart.ToString();
                edtTimeLineDateEnd.Text = TimeLine.Line.DateEnd.ToString();
                nudTimeLineInterval.Value = TimeLine.Line.IntervalValue.HasValue ? TimeLine.Line.IntervalValue.Value : 0;
                nudTimeLineLen.Maximum = TimeLine.Line.Len * 5;
                nudTimeLineLen.Value = TimeLine.Line.Len;
                cbTimeLineTimePart.SelectedIndex = TimeLine.Line.IdTimepart.HasValue ? TimeLine.Line.IdTimepart.Value : -1;
                ResizeZone(new Rectangle(new Point(0,0), new Size(TimeLine.Line.Len, TimeLine.Line.Y + Consts.TimelineHeight)));
                TimeLine.SetTimelineBlock(new Point(0, TimeLine.Line.Y - 1), new Size(TimeLine.Line.Len, Consts.TimelineHeight), getMyPen(true));
                TimeLine.blocks.LastChanged += BlockReSelecting;
                edtTimeLineDateStart.TextChanged += TimeLineReSection;
                edtTimeLineDateEnd.TextChanged += TimeLineReSection;
                nudTimeLineLen.ValueChanged += TimeLineReSection;
                nudTimeLineInterval.ValueChanged += TimeLineReSection;
                cbTimeLineTimePart.SelectedIndexChanged += TimeLineReSection;
            }
            repaint();
            
        }

        #region ' конструктор'
        public Form1()
        {

            InitializeComponent();
            DoubleBuffered = true;
            Text = Consts.Title;

            btnSelect.Checked = true;
            KeyPreview = true;

            // установка подсказок в тулбаре
            foreach (ToolStripItem item in toolStrip1.Items)
                if (item is ToolStripButton || item is ToolStripDropDownButton)
                    item.Text = Consts.ToolTips[item.Name.Substring(3)];


            System.Drawing.Point PreviousPoint = System.Drawing.Point.Empty;

            ZoneSize = new Size(Zone.Size.Width, Zone.Size.Height);

            clearMyAll();

            btnColor.BackColor = colorDialog1.Color;
            TimeLine.TimeLineChanged += TimeLineChanged;
            Zone.Hide();
            Loader.Load();
            SetupAccordion();
            
        }

        private Control AddAccPart(TableLayoutPanel tlp, string info, Control control, int pos, bool dofilldock)
        {
            tlp.Controls.Add(new Label { Width = tlp.Width, Text = info, TextAlign = ContentAlignment.BottomLeft }, 0, pos);
            if (dofilldock)
                control.Dock = DockStyle.Fill;
            tlp.Controls.Add(control, 0, pos + 1);
            return control;
        }

        public void SetupAccordion()
        {
            acc.AnimateOpenEffect = AnimateWindowFlags.None;
            acc.AnimateCloseEffect = AnimateWindowFlags.None;
            int pos = 0;
            tlpTimeline = new TableLayoutPanel { Dock = DockStyle.Fill };
            tlpTimeline.TabStop = true;
            tlpTimeline.Enabled = false;
            edtTimeLineName = (TextBox)AddAccPart(tlpTimeline, Consts.FieldName, new TextBox(), pos, true);
            edtTimeLineMemo = (TextBox)AddAccPart(tlpTimeline, Consts.FieldInfo, new TextBox { Multiline = true, Height = 50 }, pos += 2, true);
            edtTimeLineDateStart = (TextBox)AddAccPart(tlpTimeline, Consts.FieldDateStart, new TextBox(), pos += 2, true);
            edtTimeLineDateEnd = (TextBox)AddAccPart(tlpTimeline, Consts.FieldDateEnd, new TextBox(), pos += 2, true);
            nudTimeLineLen = (NumericUpDown)AddAccPart(tlpTimeline, "Длина хроники, пикс.", new NumericUpDown { Maximum = 20000 }, pos += 2, true);
            nudTimeLineInterval = (NumericUpDown)AddAccPart(tlpTimeline, "Интервал временных отрезков", new NumericUpDown { Maximum = 1000 }, pos += 2, true);
            cbTimeLineTimePart = (ComboBox)AddAccPart(tlpTimeline, "Мера временных отрезков", new ComboBox { DropDownStyle = ComboBoxStyle.DropDownList }, pos += 2, true);
            cbTimeLineTimePart.Items.AddRange(TimePart.GetTimeParts().Select(x => new ComboBoxItem(x.Id, x.Name)).ToArray());
            acc.Add(tlpTimeline, Consts.TuneTimeline, null, 0, false);

            tlpEvent = new TableLayoutPanel { Dock = DockStyle.Fill };
            tlpEvent.TabStop = true;
            tlpEvent.Enabled = false;
            pos = 0;
            edtEventName = (TextBox)AddAccPart(tlpEvent, Consts.FieldName, new TextBox(), pos, true);
            edtEventMemo = (TextBox)AddAccPart(tlpEvent, Consts.FieldInfo, new TextBox { Multiline = true, Height = 50 }, pos += 2, true);
            edtEventDateStart = (TextBox)AddAccPart(tlpEvent, Consts.FieldDateStart, new TextBox(), pos += 2, true);
            edtEventDateEnd = (TextBox)AddAccPart(tlpEvent, Consts.FieldDateEnd, new TextBox(), pos += 2, true);
            btnFitEventToDate = new Button { Text = Consts.FitEventToDate, Dock = DockStyle.Fill };
            btnFitEventToDate.Click += FitEventToDate;
            tlpEvent.Controls.Add(btnFitEventToDate, 0, pos += 2);
            acc.Add(tlpEvent, Consts.TuneEvent, null, 0, false);
            edtEventName.Leave += EventInputsLeave;
            edtEventMemo.Leave += EventInputsLeave;
            edtEventDateStart.Leave += EventInputsLeave;
            edtEventDateEnd.Leave += EventInputsLeave;

            tlpStoryline = new TableLayoutPanel { Dock = DockStyle.Fill };
            tlpStoryline.TabStop = true;
            tlpStoryline.Enabled = false;
            pos = 0;
            edtStoryLineName = (TextBox)AddAccPart(tlpStoryline, Consts.FieldName, new TextBox(), pos, true);
            edtStoryLineMemo = (TextBox)AddAccPart(tlpStoryline, Consts.FieldInfo, new TextBox { Multiline = true, Height = 50 }, pos += 2, true);
            edtStoryLineFullName = (TextBox)AddAccPart(tlpStoryline, Consts.FieldFullName, new TextBox(), pos += 2, true);
            edtStoryLineType = (TextBox)AddAccPart(tlpStoryline, Consts.StorylineType, new TextBox(), pos += 2, true);
            edtStoryLineNotes = (TextBox)AddAccPart(tlpStoryline, Consts.FieldNotes, new TextBox(), pos += 2, true);
            acc.Add(tlpStoryline, Consts.TuneStoryLines, null, 0, false);
            edtStoryLineName.Leave += StorylineInputsLeave;
            edtStoryLineMemo.Leave += StorylineInputsLeave;
            edtStoryLineFullName.Leave += StorylineInputsLeave;
            edtStoryLineType.Leave += StorylineInputsLeave;
            edtStoryLineNotes.Leave += StorylineInputsLeave;

            tlpPerson = new TableLayoutPanel { Dock = DockStyle.Fill };
            tlpPerson.TabStop = true;
            tlpPerson.Enabled = false;
            lstPersons = new ListView { View = View.Details, Dock = DockStyle.Fill };
            lstPersons.Columns.Add(Consts.PersonName, 70);
            lstPersons.Columns.Add(Consts.FieldInfo, 140);
            tlpPerson.Controls.Add(lstPersons, 0, 0);
            btnManagePerson = new Button { Text = Consts.Tune };
            btnManagePerson.Click += ManagePerson;
            tlpPerson.Controls.Add(btnManagePerson, 0, 1);
            acc.Add(tlpPerson, Consts.TunePerson, null, 0, false);

            tlpArea = new TableLayoutPanel { Dock = DockStyle.Fill };
            tlpArea.TabStop = true;
            tlpArea.Enabled = false;
            lstAreas = new ListView { View = View.Details, Dock = DockStyle.Fill };
            lstAreas.Columns.Add(Consts.FieldName, 70);
            lstAreas.Columns.Add(Consts.FieldInfo, 140);
            tlpArea.Controls.Add(lstAreas, 0, 0);
            btnManageArea = new Button { Text = Consts.Tune };
            btnManageArea.Click += ManageArea;
            tlpArea.Controls.Add(btnManageArea, 0, 1);
            acc.Add(tlpArea, Consts.TuneArea, null, 0, false);

            tlpImage = new TableLayoutPanel { Dock = DockStyle.Fill };
            tlpImage.TabStop = true;
            tlpImage.Enabled = false;
            lstImages = new ListView { View = View.Details, Dock = DockStyle.Fill };
            lstImages.Columns.Add(Consts.FieldName, 70);
            lstImages.Columns.Add(Consts.FieldInfo, 140);
            tlpImage.Controls.Add(lstImages, 0, 0);
            btnManageImage = new Button { Text = Consts.Tune };
            btnManageImage.Click += ManageImage;
            tlpImage.Controls.Add(btnManageImage, 0, 1);
            acc.Add(tlpImage, Consts.TuneImages, null, 0, false);


        }

        public void TimeLineReSection(object sender, EventArgs e)
        {
            ModifyTimeline();
            repaint();
        }

        public void FitEventToDate(object sender, EventArgs e)
        {
            Event evt = TimeLine.Line.EventFromBlock(blocks.Last());
            if (evt.DateStart.HasValue && evt.DateStart.HasValue && TimeLine.Line.DateEnd.HasValue && TimeLine.Line.DateStart.HasValue)
            {
                DateTime e1 = evt.DateStart.Value;
                DateTime e2 = evt.DateEnd.Value;
                DateTime t1 = TimeLine.Line.DateStart.Value;
                DateTime t2 = TimeLine.Line.DateEnd.Value;
                if (e1 >= t1 && e1 < e2 && e2 <= t2)
                {
                    TimeSpan line = TimeLine.Line.DateEnd.Value - TimeLine.Line.DateStart.Value;
                    TimeSpan ev = e1 - TimeLine.Line.DateStart.Value;
                    blocks.Last().SetX((int)Math.Round(TimeLine.Line.Len * ev.TotalSeconds / line.TotalSeconds));
                }
                repaint();
            }
        }

        private void StorylineInputsLeave(object sender, EventArgs e)
        {
            TimeLine.Line.StoryLineFromBlock(blocks.Last()).ModifySpecs(edtStoryLineName.Text, edtStoryLineMemo.Text,
                edtStoryLineFullName.Text, edtStoryLineType.Text, edtStoryLineNotes.Text);
            blocks.Last().SetDataValue(edtStoryLineName.Text);
        }

        private void EventInputsLeave(object sender, EventArgs e)
        {
            DateTime dt;
            TimeLine.Line.EventFromBlock(blocks.Last()).ModifySpecs(edtEventName.Text, edtEventMemo.Text,
                DateTime.TryParse(edtEventDateStart.Text, out dt) ? dt : (DateTime?)null,
                DateTime.TryParse(edtEventDateEnd.Text, out dt) ? dt : (DateTime?)null);
            blocks.Last().SetDataValue(edtEventName.Text);
        }

        void FillInfos(int target = 0)
        {
            Event ev = TimeLine.Line.EventFromBlock(blocks.Last());

            if (target == 0 || target == 1)
            {
                lstPersons.Items.Clear();
                foreach (Person data in Person.GetPersons())
                    if (data.IsEventPerson(ev.Id))
                    {
                        ListViewItem item = new ListViewItem();
                        item.Text = Convert.ToString(data.Name);
                        item.SubItems.Add(Convert.ToString(data.Memo));
                        lstPersons.Items.Add(item);
                    }
            }
            if (target == 0 || target == 2)
            {
                lstAreas.Items.Clear();
                foreach (Area data in Area.GetAreas())
                    if (data.IsEventArea(ev.Id))
                    {
                        ListViewItem item = new ListViewItem();
                        item.Text = Convert.ToString(data.Name);
                        item.SubItems.Add(Convert.ToString(data.Memo));
                        lstAreas.Items.Add(item);
                    }
            }
            if (target == 0 || target == 3)
            {
                lstImages.Items.Clear();
                foreach (dbImage data in dbImage.GetImages())
                    if (data.IsImageBound(0, ev.Id))
                    {
                        ListViewItem item = new ListViewItem();
                        item.Text = Convert.ToString(data.Name);
                        item.SubItems.Add(Convert.ToString(data.Memo));
                        lstImages.Items.Add(item);
                    }
            }
        }

        public void BlockReSelecting(BlockObject block)
        {
            bool inactive = blocks.Last() == null;
            bool forevent = false;
            bool forstory = false;
            if (!inactive)
            {
                forstory = blocks.Last().IsLine;
                forevent = !blocks.Last().IsLine && !blocks.Last().IsTimeLine;
            }

            tlpArea.Enabled = forevent;
            tlpPerson.Enabled = forevent;
            tlpEvent.Enabled = forevent;
            tlpImage.Enabled = forevent;
            tlpStoryline.Enabled = forstory;
            lstAreas.Items.Clear();
            lstPersons.Items.Clear();
            lstImages.Items.Clear();
            edtEventName.Text = "";
            edtEventMemo.Text = "";
            edtEventDateStart.Text = "";
            edtEventDateEnd.Text = "";
            edtStoryLineName.Text = "";
            edtStoryLineMemo.Text = "";
            edtStoryLineFullName.Text = "";
            edtStoryLineType.Text = "";
            edtStoryLineNotes.Text = "";

            if (inactive)
                return;
            if (forevent)
            {
                Event ev = TimeLine.Line.EventFromBlock(blocks.Last());
                edtEventName.Text = ev.Name;
                edtEventMemo.Text = ev.Memo;
                edtEventDateStart.Text = ev.DateStart.ToString();
                edtEventDateEnd.Text = ev.DateEnd.ToString();
                FillInfos();
            }
            if (forstory)
            {
                Storyline sl = TimeLine.Line.StoryLineFromBlock(blocks.Last());
                edtStoryLineName.Text = sl.Name;
                edtStoryLineMemo.Text = sl.Memo;
                edtStoryLineFullName.Text = sl.FullName;
                edtStoryLineType.Text = sl.StorylineType;
                edtStoryLineNotes.Text = sl.Notes;
            }
        }

        private void ManagePerson(object sender, EventArgs e)
        {

            PersonsForm form = new PersonsForm();
            form.Open(TimeLine.Line.EventFromBlock(blocks.Last()));
            FillInfos(1);
        }

        private void ManageArea(object sender, EventArgs e)
        {
            AreasForm form = new AreasForm();
            form.Open(TimeLine.Line.EventFromBlock(blocks.Last()));
            FillInfos(2);
        }

        private void ManageImage(object sender, EventArgs e)
        {
            ImagesForm form = new ImagesForm();
            Event ev = TimeLine.Line.EventFromBlock(blocks.Last());
            form.Open(0, ev.Id, ev.Name);
            FillInfos(3);
        }

        #endregion

        #region ' события мыши на pictureBox '

        /// <summary>
        /// нажатие кнопки мыши 
        /// </summary>        
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            // запоминаем стартовую точку
            PreviousPoint.X = e.X;
            PreviousPoint.Y = e.Y;

            if (mode == MouseMode.Move) // перемещатель
            {
                if (blocks.StartMove(PreviousPoint))
                    repaint();
            }
            else if (mode == MouseMode.AddLink) // создание связи
            {
                string blockAnswer = blocks.StartLink(PreviousPoint, getMyPen(true));
                if (!string.IsNullOrEmpty(blockAnswer))
                    MessageBox.Show(blockAnswer);
            }
            else if (mode == MouseMode.Select) // выделение
            {
                if (blocks.StartSelection(PreviousPoint))
                    repaint();
            }
            else if (mode == MouseMode.AddLine)
            {

            }

            if (bmp != null) bmp.Dispose();
            bmp = new Bitmap(Zone.Image);
        }

        /// <summary>
        /// движение мыши
        /// </summary>
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (MouseButtons.Left == e.Button) // левмышь нажата
            {
                // настройка области выделения
                Pen myPen = getMyPen(true);
                Pen SelPenAll = getMyPen();
                Pen SelPenAny = getMyPen();
                SelPenAny.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;

                if (blocks.moveBlock) // управление движением блоков
                {
                    blocks.Move(new Point(e.X, e.Y), g, Zone, repaint, Cursor);
                }
                else if (blocks.resizeBlock) // управление изменением размеров
                {
                    blocks.Resize(new Point(e.X, e.Y), PreviousPoint, g, Zone, repaint);
                }
                else if (blocks.rightNowSelect) // область выделения
                {
                    if (PreviousPoint.X > e.X)
                        drawMyRect(e, SelPenAny);
                    else
                        drawMyRect(e, SelPenAll);
                }
                else // рисование в данный момент нового блока
                {
                    switch (mode)
                    {
                        case MouseMode.AddRect:
                        case MouseMode.AddElps:
                        case MouseMode.AddRomb:
                        case MouseMode.AddHex:
                        case MouseMode.AddLine:
                            MyDraw(e, myPen, Consts.ModeToBlock[mode]);
                            break;
                        case MouseMode.AddLink:
                            if (blocks.rightNowBuildLink)
                            {
                                drawMyLink(e, myPen);
                            }
                            break;
                        default:
                            break;
                    }
                }
                myPen.Dispose();
                Zone.Invalidate();
            }
            else // левмышь не нажата
            {
                if (mode == MouseMode.Move) // перемещение
                {
                    Point curPoint = new Point(e.X, e.Y);
                    bool needSizeAllCursor = true;
                    if (checkInsiding(curPoint))
                    {
                        BlockObject ourBlock = blocks.getCurrentBlockObject(curPoint);
                        if (ourBlock != null)
                        {
                            if (ourBlock.isInsideResArea(curPoint))
                            {
                                if (this.Cursor != Cursors.SizeNWSE) { this.Cursor = Cursors.SizeNWSE; }
                                needSizeAllCursor = false;
                            }
                        }
                    }
                    if (needSizeAllCursor) { if (this.Cursor != Cursors.SizeAll) { this.Cursor = Cursors.SizeAll; } }
                }
            }
        }

        /// <summary>
        /// отжатие мыши
        /// </summary>
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (mode == MouseMode.Move && blocks.movableBlock != null) // режим движения
            {
                blocks.Drop(new Point(e.X, e.Y));
                ResizeZone(blocks.movableBlock.Rect);
                repaint();
            }
            else if (blocks.rightNowBuildLink) // создание связи
            {
                eraseTemplategraphics();
                string blockAnswer = blocks.EndLink(new Point(e.X, e.Y), g, Zone);
                if (!string.IsNullOrEmpty(blockAnswer))
                    MessageBox.Show(blockAnswer);
            }
            else if (mode == MouseMode.AddLink) { } // пытались сделать ссылку, но не вышло
            else if (mode == MouseMode.Select) // выделение
            {
                if (blocks.rightNowSelect)
                {
                    blocks.rightNowSelect = false;
                    eraseTemplategraphics();
                    Rectangle selection = new Rectangle(Math.Min(PreviousPoint.X, e.X), Math.Min(PreviousPoint.Y, e.Y), 0, 0);
                    selection.Width = Math.Max(PreviousPoint.X, e.X) - selection.Left;
                    selection.Height = Math.Max(PreviousPoint.Y, e.Y) - selection.Top;
                    foreach (BlockObject one in blocks)
                        if ((PreviousPoint.X > e.X && selection.IntersectsWith(one.Rect)) ||
                                (PreviousPoint.X <= e.X && selection.Contains(one.Rect)))
                            one.Selected = true;
                    repaint();
                }
                blocks.EndSelection();
            }
            else if (mode == MouseMode.AddData) // управление данными блока
            {
                if (blocks.EditReady(new Point(e.X, e.Y)))
                    dataInputStart();
            }
            else if (mode == MouseMode.AddLine) // вставка сюжлинии
            {
                blocks.Add(new BlockObject(Consts.ModeToBlock[mode], new Point(0, PreviousPoint.Y), new Size(Zone.Width, Consts.BlockSize.Height), getMyPen(true)));
                repaint();
            }
            else if (mode == MouseMode.Move)
            { }
            else // только что нарисовали какую-то фигуру, зарегистрируем
            {
                blocks.Add(new BlockObject(Consts.ModeToBlock[mode], PreviousPoint, Consts.BlockSize, getMyPen(true)));
                repaint();
            }

        }

        /// <summary>
        /// ресайз рабочей зоны
        /// </summary>
        private void ResizeZone(Rectangle candidate)
        {
            if (ZoneSize.Width < candidate.Right || ZoneSize.Height < candidate.Bottom)
                ZoneSize = new Size(Math.Max(candidate.Right, ZoneSize.Width),
                    Math.Max(candidate.Bottom, ZoneSize.Height));
        }
        #endregion

        #region ' работа с тулбаром'

        /// <summary>
        /// кнопки-инструменты с режимом
        /// </summary>
        void ModeButtonClick(object sender, EventArgs e)
        {
            bool isModeBtn = true;
            string modeName = ((ToolStripButton)sender).Name.Substring(3);
            int idx = (new List<string>(Enum.GetNames(typeof(MouseMode))).IndexOf(modeName));
            if (idx != -1)
                mode = (MouseMode)(Enum.GetValues(typeof(MouseMode)).GetValue(idx));
            else
                isModeBtn = false;
            foreach (ToolStripItem item in ((ToolStripButton)sender).GetCurrentParent().Items)
                if (item is ToolStripButton && isModeBtn)
                {
                    ToolStripButton btn = (ToolStripButton)item;
                    if (item == sender)
                        btn.Checked = true;
                    if (item != null && item != sender && btn.Checked == true)
                        btn.Checked = false;
                }
            if (isModeBtn)
                Cursor = Consts.ModeCursors[mode];
            repaint();
        }

        /// <summary>
        ///  выбор цвета
        /// </summary>
        private void btnColor_Click(object sender, EventArgs e)
        {
            if (blocks == null)
                return;
            colorDialog1.ShowDialog();
            btnColor.BackColor = colorDialog1.Color;
            foreach (BlockObject one in blocks)
                if (one.Selected)
                    one.blockPen.Color = colorDialog1.Color;
            repaint();
        }

        /// <summary>
        /// очистить всё
        /// </summary>
        private void btnClear_Click(object sender, EventArgs e)
        {
            if (Prompt.Confirm("Действительно очистить всю схему?", "Подтверждение"))
            {
                clearMyAll();
            }
        }

        /// <summary>
        /// удалить выделенные
        /// </summary>
        private void btnRemove_Click(object sender, EventArgs e)
        {
            DeleteBlock();
        }

        /// <summary>
        /// удалить все ссылки у выделенных
        /// </summary>
        private void btnRemoveLinks_Click(object sender, EventArgs e)
        {
            if (blocks == null)
                return;
            foreach (BlockObject one in blocks)
            {
                if (one.Selected) { one.RemoveLinks(); }
            }
            repaint();
        }

        private void btnWidth_Click(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem item in ((ToolStripMenuItem)sender).GetCurrentParent().Items)
            {
                if (blocks == null)
                    return;
                if (item == sender)
                    item.Checked = true;
                if (item != null && item != sender && item.Checked == true)
                    item.Checked = false;
                PenSize = int.Parse(((ToolStripMenuItem)sender).Text);
                foreach (BlockObject one in blocks)
                    if (one.Selected)
                        one.blockPen.Width = PenSize;
            }
        }

        #endregion

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            ResizeZone(ZoneHolder.ClientRectangle);
        }

        #region ' процессы очистки '
        /// <summary>
        /// полная очистка
        /// </summary> 
        private void clearMyAll()
        {
            clearMyCanvas();
            if (TimeLine.Line != null)
                TimeLine.Line.Clear(false);
            calcCounts();
            repaint();
        }

        /// <summary>
        /// очистка канвы (pictureBox)
        /// </summary>
        private void clearMyCanvas()
        {
            SolidBrush sb = new SolidBrush(Color.White);
            g = Graphics.FromImage(Zone.Image);
            g.FillRectangle(sb, 0, 0, Zone.Width, Zone.Height);
            g.Dispose();
            Zone.Invalidate();
        }

        /// <summary>
        /// стирание временной копии нарисованного
        /// </summary>
        private void eraseTemplategraphics()
        {
            if (Zone.Image != null) Zone.Image.Dispose();
            Zone.Image = new Bitmap(bmp);
        }
        #endregion

        #region ' процессы рисования '

        /// <summary>
        /// перерисование всех блоков
        /// </summary>
        private void repaint()
        {
            if (blocks == null)
                return;
            clearMyCanvas();
            if (bmp != null) bmp.Dispose();
            bmp = new Bitmap(Zone.Image);
            Zone.Image = new Bitmap(bmp);

            Pen myPen = getMyPen();
            ArrayList links = new ArrayList();

            if (Options.ShowEventLines)
            {
                g = Graphics.FromImage(Zone.Image);
                foreach (BlockObject one in blocks)
                    if (!one.IsLine && !one.IsTimeLine)
                    {
                        TimeLine tl = TimeLine.Line;
                        Event evt = tl.EventFromBlock(one);
                        if (evt.DateStart.HasValue && evt.DateEnd.HasValue && tl.DateEnd.HasValue && tl.DateStart.HasValue)
                        {
                            DateTime e1 = evt.DateStart.Value;
                            DateTime e2 = evt.DateEnd.Value;
                            DateTime t1 = tl.DateStart.Value;
                            DateTime t2 = tl.DateEnd.Value;
                            if (e1 >= t1 && e1 < e2 && e2 <= t2)
                            {
                                TimeSpan line = tl.DateEnd.Value - tl.DateStart.Value;
                                TimeSpan ev = e1 - tl.DateStart.Value;
                                g.DrawLine(new Pen(Color.Black), one.X, one.Y + one.Height / 2,
                                    ((int)Math.Round(tl.Len * ev.TotalSeconds / line.TotalSeconds)),
                                    Zone.Height - Consts.TimelineHeight);
                            }
                        }
                    }
            }

            foreach (BlockObject one in blocks)
            {
                one.DrawIt(g, Zone, mode == MouseMode.Move);
                // Рисуем связи
                foreach (BlocksLink lnk in one.Links)
                {
                    if (!(links.Contains(lnk)))
                    {
                        links.Add(lnk);
                        lnk.DrawIt(g, Zone);
                    }
                }
                Zone.Invalidate();
            }
            myPen.Dispose();
            calcCounts();
        }

        private void MyDraw(MouseEventArgs e, Pen myPen, BlockType block)
        {
            int w = 0, h = 0;
            System.Drawing.Point point = System.Drawing.Point.Empty;

            eraseTemplategraphics();

            g = Graphics.FromImage(Zone.Image);
            calcDrawPoints(e, ref point, ref w, ref h);

            Point[] points = null;
            switch (block)
            {
                case BlockType.Romb:
                    points = new Point[]
                    {
                        new Point(point.X, point.Y + h / 2),
                        new Point(point.X + w / 2, point.Y),
                        new Point(point.X + w, point.Y + h / 2),
                        new Point(point.X + w / 2, point.Y + h)
                    };
                    break;
                case BlockType.Hex:
                    points = new Point[]
                    {
                        new Point(point.X, point.Y + h / 2),
                        new Point(point.X + w / 4, point.Y),
                        new Point(point.X + 3 * w / 4, point.Y),
                        new Point(point.X + w, point.Y + h / 2),
                        new Point(point.X + 3 * w / 4, point.Y + h),
                        new Point(point.X + w / 4, point.Y + h)
                    };
                    break;
                default:
                    break;
            }

            switch (block)
            {
                case BlockType.Rect:
                    g.DrawRectangle(myPen, point.X, point.Y, w, h);
                    break;
                case BlockType.Line:
                    g.DrawRectangle(myPen, 0, point.Y, Zone.Width, h);
                    break;
                case BlockType.Elps:
                    g.DrawEllipse(myPen, point.X, point.Y, w, h);
                    break;
                case BlockType.Romb:
                    g.DrawPolygon(myPen, points);
                    break;
                case BlockType.Hex:
                    g.DrawPolygon(myPen, points);
                    break;
                default:
                    throw new ArgumentException(block.ToString() + " blocktype is not supported for drawing");
            }

            g.Dispose();
        }

        /// <summary>
        /// рисование прямоугольника
        /// </summary>
        private void drawMyRect(MouseEventArgs e, Pen myPen)
        {
            MyDraw(e, myPen, BlockType.Rect);
        }

        /// <summary>
        /// получение координат для рисования блоков
        /// </summary>
        private void calcDrawPoints(MouseEventArgs e, ref Point point, ref int w, ref int h)
        {
            w = e.X - PreviousPoint.X;
            h = e.Y - PreviousPoint.Y;
            if (e.X < PreviousPoint.X)
            {
                point.X = e.X;
                w = PreviousPoint.X - e.X;
            }
            else point.X = PreviousPoint.X;
            if (e.Y < PreviousPoint.Y)
            {
                point.Y = e.Y;
                h = PreviousPoint.Y - e.Y;
            }
            else point.Y = PreviousPoint.Y;
        }

        /// <summary>
        /// рисование связи при свободном конце
        /// </summary>
        private void drawMyLink(MouseEventArgs e, Pen myPen)
        {
            if (blocks.rightNowBuildLink)
            {
                Point point = new Point(e.X, e.Y);
                Point pointS = blocks.curLink.GetStartPoint(point);
                eraseTemplategraphics();
                g = Graphics.FromImage(Zone.Image);
                float penWidth = myPen.Width;
                myPen.Width = PenSize;
                g.DrawLine(myPen, pointS, point);
                myPen.Width = penWidth;
                g.Dispose();
            }
        }

        /// <summary>
        /// получение пера
        /// </summary>
        private Pen getMyPen(bool withSize = false)
        {
            Pen myPen = new Pen(colorDialog1.Color, withSize ? PenSize : 1f);
            myPen.StartCap = System.Drawing.Drawing2D.LineCap.Round;
            myPen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
            return myPen;
        }
        #endregion

        /// <summary>
        /// процесс - проверка переданной на нахождение в одном из блоков
        /// </summary>
        private bool checkInsiding(Point cp)
        {
            bool insidenow = false;
            foreach (BlockObject one in blocks)
            {
                if (one.isInside(cp)) { insidenow = true; break; }
            }
            return insidenow;
        }

        /// <summary>
        /// подсчет количества блоков и выделенных
        /// </summary>
        private void calcCounts()
        {
            //Text = blocks.Amount(false).ToString();
            //Text = blocks.Amount(true).ToString();
        }


        private void dataInputStart()
        {
            edtEventName.Text = blocks.curBlock.DataValue;

        }

        #region удаление блоков
        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (!acc.CanFocus && e.KeyCode == Keys.Delete)
                DeleteBlock();
        }

        private void deleteBlockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteBlock();
        }

        private void DeleteBlock()
        {
            if (blocks == null)
                return;
            if (Prompt.Confirm("Удалить блок?", ""))
            {
                ArrayList deadmans = new ArrayList();
                foreach (BlockObject one in blocks)
                {
                    if (one.Selected)
                    {
                        one.RemoveLinks();
                        deadmans.Add(one);
                    }
                }
                foreach (BlockObject corpse in deadmans)
                {
                    blocks.RemoveBlock(corpse);
                }
                repaint();
            }
        }
        #endregion

        #region ' меню '



        private void menuItemNew_Click(object sender, EventArgs e)
        {
            if (TimeLine.Line == null || Prompt.Confirm("Создание новой хроники закроет текущую без сохранения, продолжить ?", "Внимание!"))
            {
                string name = Prompt.ShowDialog("Введите имя хроники", "Новая хроника");
                if (name.Trim() != "")
                    TimeLine.Line = TimeLine.Create(name);
            }
        }

        private void menuItemManage_Click(object sender, EventArgs e)
        {
            TimelinesForm form = new TimelinesForm();
            form.ShowDialog();
        }

        private void ModifyTimeline()
        {
            if (TimeLine.Line == null)
                return;
            DateTime date;
            TimeLine.Line.Modify(
                edtTimeLineName.Text,
                DateTime.TryParse(edtTimeLineDateStart.Text, out date) ? date : (DateTime?)null,
                DateTime.TryParse(edtTimeLineDateEnd.Text, out date) ? date : (DateTime?)null,
                edtTimeLineMemo.Text,
                (int)nudTimeLineLen.Value,
                (int)nudTimeLineInterval.Value,
                cbTimeLineTimePart.SelectedIndex == -1 ? (int?)null : cbTimeLineTimePart.SelectedIndex);
        }

        private void MenuItemSave_Click(object sender, EventArgs e)
        {
            ModifyTimeline();
            TimeLine.Line.Save();
        }



        private void menuItemQuit_Click(object sender, EventArgs e)
        {
            if (Prompt.Confirm("Несохранённые изменения будут потеряны. Выйти?", ""))
                Close();
        }

        private void menuBlockHeightResize_Click(object sender, EventArgs e)
        {
            Options.DoBlockHeightResize = !Options.DoBlockHeightResize;
        }

        private void menuRelativeTimeline_Click(object sender, EventArgs e)
        {
            Options.RelativeTimelineSections = !Options.RelativeTimelineSections;
            repaint();
        }


        private void menuItemAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Consts.Title, Consts.Title);
        }

        private void menuEventLines_Click(object sender, EventArgs e)
        {
            Options.ShowEventLines = !Options.ShowEventLines;
            repaint();
        }

        #endregion ' меню '

    }
}
