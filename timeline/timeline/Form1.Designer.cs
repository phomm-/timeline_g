﻿using System.Drawing;

namespace Timeline
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DBPanel = new System.Windows.Forms.Panel();
            this.acc = new Opulos.Core.UI.Accordion();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.ttipOnForm = new System.Windows.Forms.ToolTip(this.components);
            this.ZoneHolder = new System.Windows.Forms.Panel();
            this.Zone = new System.Windows.Forms.PictureBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemNew = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemManage = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemQuit = new System.Windows.Forms.ToolStripMenuItem();
            this.EditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuBlockHeightResize = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRelativeTimeline = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEventLines = new System.Windows.Forms.ToolStripMenuItem();
            this.ServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.HelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnSelect = new System.Windows.Forms.ToolStripButton();
            this.btnMove = new System.Windows.Forms.ToolStripButton();
            this.btnAddData = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAddHex = new System.Windows.Forms.ToolStripButton();
            this.btnAddRomb = new System.Windows.Forms.ToolStripButton();
            this.btnAddElps = new System.Windows.Forms.ToolStripButton();
            this.btnAddRect = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAddPanel = new System.Windows.Forms.ToolStripButton();
            this.btnAddLine = new System.Windows.Forms.ToolStripButton();
            this.btnAddLink = new System.Windows.Forms.ToolStripButton();
            this.btnAddPerson = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnColor = new System.Windows.Forms.ToolStripButton();
            this.btnPenWidth = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.bntDelBlock = new System.Windows.Forms.ToolStripButton();
            this.btnDelLink = new System.Windows.Forms.ToolStripButton();
            this.btnClear = new System.Windows.Forms.ToolStripButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.contextMenuBlock = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteBlockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DBPanel.SuspendLayout();
            this.ZoneHolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Zone)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.contextMenuBlock.SuspendLayout();
            this.SuspendLayout();
            // 
            // DBPanel
            // 
            this.DBPanel.Controls.Add(this.acc);
            this.DBPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.DBPanel.Location = new System.Drawing.Point(0, 0);
            this.DBPanel.Name = "DBPanel";
            this.DBPanel.Size = new System.Drawing.Size(220, 568);
            this.DBPanel.TabIndex = 1;
            // 
            // acc
            // 
            this.acc.AddResizeBars = true;
            this.acc.AllowMouseResize = true;
            this.acc.AnimateCloseEffect = ((Opulos.Core.UI.AnimateWindowFlags)(((Opulos.Core.UI.AnimateWindowFlags.VerticalNegative | Opulos.Core.UI.AnimateWindowFlags.Hide) 
            | Opulos.Core.UI.AnimateWindowFlags.Slide)));
            this.acc.AnimateCloseMillis = 300;
            this.acc.AnimateOpenEffect = ((Opulos.Core.UI.AnimateWindowFlags)(((Opulos.Core.UI.AnimateWindowFlags.VerticalPositive | Opulos.Core.UI.AnimateWindowFlags.Show) 
            | Opulos.Core.UI.AnimateWindowFlags.Slide)));
            this.acc.AnimateOpenMillis = 300;
            this.acc.AutoFixDockStyle = true;
            this.acc.AutoScroll = true;
            this.acc.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.acc.CheckBoxFactory = null;
            this.acc.CheckBoxMargin = new System.Windows.Forms.Padding(0);
            this.acc.ContentBackColor = null;
            this.acc.ContentMargin = null;
            this.acc.ContentPadding = new System.Windows.Forms.Padding(5);
            this.acc.ControlBackColor = null;
            this.acc.ControlMinimumHeightIsItsPreferredHeight = true;
            this.acc.ControlMinimumWidthIsItsPreferredWidth = true;
            this.acc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.acc.DownArrow = null;
            this.acc.FillHeight = true;
            this.acc.FillLastOpened = false;
            this.acc.FillModeGrowOnly = false;
            this.acc.FillResetOnCollapse = false;
            this.acc.FillWidth = true;
            this.acc.GrabCursor = System.Windows.Forms.Cursors.SizeNS;
            this.acc.GrabRequiresPositiveFillWeight = true;
            this.acc.GrabWidth = 6;
            this.acc.GrowAndShrink = true;
            this.acc.Insets = new System.Windows.Forms.Padding(0);
            this.acc.Location = new System.Drawing.Point(0, 0);
            this.acc.Name = "acc";
            this.acc.OpenOnAdd = false;
            this.acc.OpenOneOnly = false;
            this.acc.ResizeBarFactory = null;
            this.acc.ResizeBarsAlign = 0.5D;
            this.acc.ResizeBarsArrowKeyDelta = 10;
            this.acc.ResizeBarsFadeInMillis = 800;
            this.acc.ResizeBarsFadeOutMillis = 800;
            this.acc.ResizeBarsFadeProximity = 24;
            this.acc.ResizeBarsFill = 1D;
            this.acc.ResizeBarsKeepFocusAfterMouseDrag = false;
            this.acc.ResizeBarsKeepFocusIfControlOutOfView = true;
            this.acc.ResizeBarsKeepFocusOnClick = true;
            this.acc.ResizeBarsMargin = null;
            this.acc.ResizeBarsMinimumLength = 50;
            this.acc.ResizeBarsStayInViewOnArrowKey = true;
            this.acc.ResizeBarsStayInViewOnMouseDrag = true;
            this.acc.ResizeBarsStayVisibleIfFocused = true;
            this.acc.ResizeBarsTabStop = true;
            this.acc.ShowPartiallyVisibleResizeBars = false;
            this.acc.ShowToolMenu = true;
            this.acc.ShowToolMenuOnHoverWhenClosed = false;
            this.acc.ShowToolMenuOnRightClick = true;
            this.acc.ShowToolMenuRequiresPositiveFillWeight = false;
            this.acc.Size = new System.Drawing.Size(220, 568);
            this.acc.TabIndex = 0;
            this.acc.UpArrow = null;
            // 
            // ZoneHolder
            // 
            this.ZoneHolder.AutoScroll = true;
            this.ZoneHolder.Controls.Add(this.Zone);
            this.ZoneHolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ZoneHolder.Location = new System.Drawing.Point(220, 0);
            this.ZoneHolder.Name = "ZoneHolder";
            this.ZoneHolder.Size = new System.Drawing.Size(510, 568);
            this.ZoneHolder.TabIndex = 22;
            // 
            // Zone
            // 
            this.Zone.Location = new System.Drawing.Point(0, 0);
            this.Zone.Name = "Zone";
            this.Zone.Size = new System.Drawing.Size(553, 524);
            this.Zone.TabIndex = 0;
            this.Zone.TabStop = false;
            this.Zone.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.Zone.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.Zone.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.splitter1.Location = new System.Drawing.Point(220, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 568);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileToolStripMenuItem,
            this.EditToolStripMenuItem,
            this.OptionsToolStripMenuItem,
            this.ServiceToolStripMenuItem,
            this.HelpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(730, 24);
            this.menuStrip1.TabIndex = 23;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // FileToolStripMenuItem
            // 
            this.FileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemNew,
            this.menuItemManage,
            this.MenuItemSave,
            this.menuItemQuit});
            this.FileToolStripMenuItem.Name = "FileToolStripMenuItem";
            this.FileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.FileToolStripMenuItem.Text = "Файл";
            // 
            // menuItemNew
            // 
            this.menuItemNew.Name = "menuItemNew";
            this.menuItemNew.Size = new System.Drawing.Size(204, 22);
            this.menuItemNew.Text = "Новая хроника";
            this.menuItemNew.Click += new System.EventHandler(this.menuItemNew_Click);
            // 
            // menuItemManage
            // 
            this.menuItemManage.Name = "menuItemManage";
            this.menuItemManage.Size = new System.Drawing.Size(204, 22);
            this.menuItemManage.Text = "Управление хрониками";
            this.menuItemManage.Click += new System.EventHandler(this.menuItemManage_Click);
            // 
            // MenuItemSave
            // 
            this.MenuItemSave.Name = "MenuItemSave";
            this.MenuItemSave.Size = new System.Drawing.Size(204, 22);
            this.MenuItemSave.Text = "Сохранить";
            this.MenuItemSave.Click += new System.EventHandler(this.MenuItemSave_Click);
            // 
            // menuItemQuit
            // 
            this.menuItemQuit.Name = "menuItemQuit";
            this.menuItemQuit.Size = new System.Drawing.Size(204, 22);
            this.menuItemQuit.Text = "Выход";
            this.menuItemQuit.Click += new System.EventHandler(this.menuItemQuit_Click);
            // 
            // EditToolStripMenuItem
            // 
            this.EditToolStripMenuItem.Name = "EditToolStripMenuItem";
            this.EditToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.EditToolStripMenuItem.Text = "Вставка";
            // 
            // OptionsToolStripMenuItem
            // 
            this.OptionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuBlockHeightResize,
            this.menuRelativeTimeline,
            this.menuEventLines});
            this.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem";
            this.OptionsToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.OptionsToolStripMenuItem.Text = "Настройка";
            // 
            // menuBlockHeightResize
            // 
            this.menuBlockHeightResize.CheckOnClick = true;
            this.menuBlockHeightResize.Name = "menuBlockHeightResize";
            this.menuBlockHeightResize.Size = new System.Drawing.Size(260, 22);
            this.menuBlockHeightResize.Text = "Изменять высоту блоков";
            this.menuBlockHeightResize.Click += new System.EventHandler(this.menuBlockHeightResize_Click);
            // 
            // menuRelativeTimeline
            // 
            this.menuRelativeTimeline.CheckOnClick = true;
            this.menuRelativeTimeline.Name = "menuRelativeTimeline";
            this.menuRelativeTimeline.Size = new System.Drawing.Size(260, 22);
            this.menuRelativeTimeline.Text = "Относительная временная шкала";
            this.menuRelativeTimeline.Click += new System.EventHandler(this.menuRelativeTimeline_Click);
            // 
            // menuEventLines
            // 
            this.menuEventLines.CheckOnClick = true;
            this.menuEventLines.Name = "menuEventLines";
            this.menuEventLines.Size = new System.Drawing.Size(260, 22);
            this.menuEventLines.Text = "Линии к хронике по дате события";
            this.menuEventLines.Click += new System.EventHandler(this.menuEventLines_Click);
            // 
            // ServiceToolStripMenuItem
            // 
            this.ServiceToolStripMenuItem.Name = "ServiceToolStripMenuItem";
            this.ServiceToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.ServiceToolStripMenuItem.Text = "Сервис";
            // 
            // HelpToolStripMenuItem
            // 
            this.HelpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemAbout});
            this.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem";
            this.HelpToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.HelpToolStripMenuItem.Text = "Помощь";
            // 
            // menuItemAbout
            // 
            this.menuItemAbout.Name = "menuItemAbout";
            this.menuItemAbout.Size = new System.Drawing.Size(149, 22);
            this.menuItemAbout.Text = "О программе";
            this.menuItemAbout.Click += new System.EventHandler(this.menuItemAbout_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSelect,
            this.btnMove,
            this.btnAddData,
            this.toolStripSeparator1,
            this.btnAddHex,
            this.btnAddRomb,
            this.btnAddElps,
            this.btnAddRect,
            this.toolStripSeparator3,
            this.btnAddPanel,
            this.btnAddLine,
            this.btnAddLink,
            this.btnAddPerson,
            this.toolStripSeparator2,
            this.btnColor,
            this.btnPenWidth,
            this.bntDelBlock,
            this.btnDelLink,
            this.btnClear});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(730, 25);
            this.toolStrip1.TabIndex = 24;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnSelect
            // 
            this.btnSelect.CheckOnClick = true;
            this.btnSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSelect.Image = global::Timeline.Properties.Resources.arrowbw;
            this.btnSelect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(23, 22);
            this.btnSelect.Click += new System.EventHandler(this.ModeButtonClick);
            // 
            // btnMove
            // 
            this.btnMove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMove.Image = global::Timeline.Properties.Resources.move;
            this.btnMove.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(23, 22);
            this.btnMove.Text = "toolStripButton2";
            this.btnMove.Click += new System.EventHandler(this.ModeButtonClick);
            // 
            // btnAddData
            // 
            this.btnAddData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddData.Image = global::Timeline.Properties.Resources.dtinp;
            this.btnAddData.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddData.Name = "btnAddData";
            this.btnAddData.Size = new System.Drawing.Size(23, 22);
            this.btnAddData.Text = "toolStripButton1";
            this.btnAddData.Visible = false;
            this.btnAddData.Click += new System.EventHandler(this.ModeButtonClick);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnAddHex
            // 
            this.btnAddHex.CheckOnClick = true;
            this.btnAddHex.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddHex.Image = global::Timeline.Properties.Resources.hexbw;
            this.btnAddHex.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddHex.Name = "btnAddHex";
            this.btnAddHex.Size = new System.Drawing.Size(23, 22);
            this.btnAddHex.Click += new System.EventHandler(this.ModeButtonClick);
            // 
            // btnAddRomb
            // 
            this.btnAddRomb.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddRomb.Image = global::Timeline.Properties.Resources.rhombus;
            this.btnAddRomb.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddRomb.Name = "btnAddRomb";
            this.btnAddRomb.Size = new System.Drawing.Size(23, 22);
            this.btnAddRomb.Text = "toolStripButton5";
            this.btnAddRomb.Visible = false;
            this.btnAddRomb.Click += new System.EventHandler(this.ModeButtonClick);
            // 
            // btnAddElps
            // 
            this.btnAddElps.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddElps.Image = global::Timeline.Properties.Resources.ellipse;
            this.btnAddElps.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddElps.Name = "btnAddElps";
            this.btnAddElps.Size = new System.Drawing.Size(23, 22);
            this.btnAddElps.Text = "toolStripButton6";
            this.btnAddElps.Visible = false;
            this.btnAddElps.Click += new System.EventHandler(this.ModeButtonClick);
            // 
            // btnAddRect
            // 
            this.btnAddRect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddRect.Image = global::Timeline.Properties.Resources.rect;
            this.btnAddRect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddRect.Name = "btnAddRect";
            this.btnAddRect.Size = new System.Drawing.Size(23, 22);
            this.btnAddRect.Text = "toolStripButton7";
            this.btnAddRect.Visible = false;
            this.btnAddRect.Click += new System.EventHandler(this.ModeButtonClick);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            this.toolStripSeparator3.Visible = false;
            // 
            // btnAddPanel
            // 
            this.btnAddPanel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddPanel.Image = global::Timeline.Properties.Resources.Folder;
            this.btnAddPanel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddPanel.Name = "btnAddPanel";
            this.btnAddPanel.Size = new System.Drawing.Size(23, 22);
            this.btnAddPanel.Text = "toolStripButton9";
            this.btnAddPanel.Visible = false;
            this.btnAddPanel.Click += new System.EventHandler(this.ModeButtonClick);
            // 
            // btnAddLine
            // 
            this.btnAddLine.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddLine.Image = global::Timeline.Properties.Resources.Linechart16LinesOnly;
            this.btnAddLine.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddLine.Name = "btnAddLine";
            this.btnAddLine.Size = new System.Drawing.Size(23, 22);
            this.btnAddLine.Click += new System.EventHandler(this.ModeButtonClick);
            // 
            // btnAddLink
            // 
            this.btnAddLink.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddLink.Image = global::Timeline.Properties.Resources.linkarrow;
            this.btnAddLink.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddLink.Name = "btnAddLink";
            this.btnAddLink.Size = new System.Drawing.Size(23, 22);
            this.btnAddLink.Click += new System.EventHandler(this.ModeButtonClick);
            // 
            // btnAddPerson
            // 
            this.btnAddPerson.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAddPerson.Image = global::Timeline.Properties.Resources.AddMaleUser16;
            this.btnAddPerson.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAddPerson.Name = "btnAddPerson";
            this.btnAddPerson.Size = new System.Drawing.Size(23, 22);
            this.btnAddPerson.Visible = false;
            this.btnAddPerson.Click += new System.EventHandler(this.ModeButtonClick);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnColor
            // 
            this.btnColor.BackColor = System.Drawing.Color.Black;
            this.btnColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnColor.Image = global::Timeline.Properties.Resources.brush;
            this.btnColor.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnColor.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnColor.Margin = new System.Windows.Forms.Padding(0);
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(23, 25);
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // btnPenWidth
            // 
            this.btnPenWidth.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPenWidth.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6});
            this.btnPenWidth.Image = global::Timeline.Properties.Resources.brush;
            this.btnPenWidth.Name = "btnPenWidth";
            this.btnPenWidth.Size = new System.Drawing.Size(29, 22);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Checked = true;
            this.toolStripMenuItem2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem2.Text = "1";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.btnWidth_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem3.Text = "2";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.btnWidth_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem4.Text = "3";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.btnWidth_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem5.Text = "4";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.btnWidth_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem6.Text = "5";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.btnWidth_Click);
            // 
            // bntDelBlock
            // 
            this.bntDelBlock.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bntDelBlock.Image = global::Timeline.Properties.Resources.rem;
            this.bntDelBlock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bntDelBlock.Name = "bntDelBlock";
            this.bntDelBlock.Size = new System.Drawing.Size(23, 22);
            this.bntDelBlock.Text = "toolStripButton3";
            this.bntDelBlock.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnDelLink
            // 
            this.btnDelLink.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDelLink.Image = global::Timeline.Properties.Resources.reml;
            this.btnDelLink.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDelLink.Name = "btnDelLink";
            this.btnDelLink.Size = new System.Drawing.Size(23, 22);
            this.btnDelLink.Text = "toolStripButton4";
            this.btnDelLink.Click += new System.EventHandler(this.btnRemoveLinks_Click);
            // 
            // btnClear
            // 
            this.btnClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnClear.Image = global::Timeline.Properties.Resources.deletefile16;
            this.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(23, 22);
            this.btnClear.Text = "toolStripButton10";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.splitter1);
            this.panel3.Controls.Add(this.ZoneHolder);
            this.panel3.Controls.Add(this.DBPanel);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 49);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(730, 568);
            this.panel3.TabIndex = 25;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 617);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(730, 22);
            this.statusStrip1.TabIndex = 27;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // contextMenuBlock
            // 
            this.contextMenuBlock.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteBlockToolStripMenuItem});
            this.contextMenuBlock.Name = "contextMenuBlock";
            this.contextMenuBlock.Size = new System.Drawing.Size(140, 26);
            // 
            // deleteBlockToolStripMenuItem
            // 
            this.deleteBlockToolStripMenuItem.Name = "deleteBlockToolStripMenuItem";
            this.deleteBlockToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.deleteBlockToolStripMenuItem.Text = "Delete Block";
            this.deleteBlockToolStripMenuItem.Click += new System.EventHandler(this.deleteBlockToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 639);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "Form1";
            this.Text = "Timeline";
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            this.Resize += new System.EventHandler(this.Form1_SizeChanged);
            this.DBPanel.ResumeLayout(false);
            this.ZoneHolder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Zone)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.contextMenuBlock.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Zone;
        private System.Windows.Forms.Panel DBPanel;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.ToolTip ttipOnForm;
        private System.Windows.Forms.Panel ZoneHolder;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EditToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OptionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ServiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem HelpToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnSelect;
        private System.Windows.Forms.ToolStripButton btnAddHex;
        private System.Windows.Forms.ToolStripButton btnAddLine;
        private System.Windows.Forms.ToolStripButton btnAddLink;
        private System.Windows.Forms.ToolStripButton btnAddPerson;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuBlock;
        private System.Windows.Forms.ToolStripMenuItem deleteBlockToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnAddRomb;
        private System.Windows.Forms.ToolStripButton btnAddElps;
        private System.Windows.Forms.ToolStripButton btnAddRect;
        private System.Windows.Forms.ToolStripButton btnMove;
        private System.Windows.Forms.ToolStripButton bntDelBlock;
        private System.Windows.Forms.ToolStripButton btnDelLink;
        private System.Windows.Forms.ToolStripButton btnColor;
        private System.Windows.Forms.ToolStripButton btnAddPanel;
        private System.Windows.Forms.ToolStripButton btnClear;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.ToolStripButton btnAddData;
        private Opulos.Core.UI.Accordion acc;
        private System.Windows.Forms.ToolStripMenuItem menuItemNew;
        private System.Windows.Forms.ToolStripMenuItem menuItemManage;
        private System.Windows.Forms.ToolStripMenuItem menuItemQuit;
        private System.Windows.Forms.ToolStripMenuItem menuItemAbout;
        private System.Windows.Forms.ToolStripDropDownButton btnPenWidth;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem MenuItemSave;
        private System.Windows.Forms.ToolStripMenuItem menuBlockHeightResize;
        private System.Windows.Forms.ToolStripMenuItem menuRelativeTimeline;
        private System.Windows.Forms.ToolStripMenuItem menuEventLines;

    }
}

