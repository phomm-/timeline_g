﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Collections;
using System.Linq;

namespace Timeline
{
    /// <summary>
    /// Объектом класса BlockObject является любой блок
    /// </summary>
    public class BlockObject
    {
        #region ' переменные '

        /// <summary>
        /// Счетчики в блоках, по типам
        /// </summary>
        static private Dictionary<BlockType, int> NumberCounter = new Dictionary<BlockType, int>
            {
                {BlockType.Hex, 0}, {BlockType.Line, 0}, {BlockType.Timeline, 0}
            };
        private int Number;

        /// <summary>
        /// Тип блока
        /// </summary>
        public BlockType blockType { get; private set; }

        /// <summary>
        /// является ли блок сюжетной линией
        /// </summary>
        public bool IsLine { get { return blockType == BlockType.Line; } }

        public bool IsTimeLine { get { return blockType == BlockType.Timeline; } }

        private bool infinityLinks = false;

        // начальная точка блока (левая верхняя)
        private Point startPoint;

        // размер блока
        private Size blockSize;

        // перо для рисования блока
        public Pen blockPen { get; private set; }

        public BlockObject parent { get; set; }
        private List<BlockObject> children = new List<BlockObject>();

        public bool IsAtLine { get { return parent != null; } }
        // Тип данных:
        // 0 =, 1 число, 2 +, 3 -, 4 /, 5 *, 6 <, 7 >, 8 ==
        private int dataType = 1;
        private string dataValue = "";
        public string DataValue { get { return dataValue; } }

        public bool Selected = false;

        // связи блока с другими блоками
        public ArrayList Links = new ArrayList();
        private ArrayList outLinks = new ArrayList();
        private ArrayList inLinks = new ArrayList();

        private const int stigSize = 4;
        private const int resArea = 16;
        private const int resAreaLineDelta = 4;
        #endregion

        #region ' конструктор '
        /// <summary>
        /// Конструктор по 2 точкам
        /// </summary>
        /// <param name="blType">тип блока</param>
        /// <param name="stp">начальная точка блока</param>
        /// <param name="enp">конечная точка блока (противоположная начальной)</param>
        public BlockObject(BlockType blType, Point stp, Point enp, Pen blpen)
            : this(blType, stp, new Size(enp.X - stp.X, enp.Y - stp.Y), blpen) { }

        // 
        /// <summary>
        /// перегрузка конструктора: вместо конечной точки - объект Size
        /// </summary>
        public BlockObject(BlockType blType, Point stp, Size blsize, Pen blpen)
        {
            blockType = blType;
            startPoint = stp;
            blockSize = blsize;
            blockPen = blpen;
            blockSideCorrection();
            Number = ++NumberCounter[blType];
            switch (blType)
            {
                case BlockType.Rect:
                case BlockType.Elps:
                case BlockType.Romb:
                case BlockType.Hex:
                    dataValue = Consts.EventText + " " + Number.ToString();
                    break;
                case BlockType.Line:
                    dataValue = Consts.LineText + " " + Number.ToString();
                    break;
            }

        }
        #endregion


        #region управление вложенными блоками

        /// <summary>
        /// добавление вложенного блока
        /// </summary>
        public void AddChild(BlockObject child)
        {
            if (child == null)
                throw new NullReferenceException();
            if (child.IsLine)
                return;
            children.Add(child);
            if (child.parent != null)
                child.parent.RemoveChild(child);
            child.parent = this;
            child.startPoint.Y = Y + 1;
        }

        /// <summary>
        /// удаление себя из линии
        /// </summary>
        public void RemoveMeFromLine()
        {
            if (IsAtLine)
                parent.RemoveChild(this);
            parent = null;
        }

        /// <summary>
        /// удаление вложенного блока
        /// </summary>
        public void RemoveChild(BlockObject child)
        {
            if (child == null)
                throw new NullReferenceException();
            if (children.Contains(child))
                children.Remove(child);
            child.parent = null;
        }
        #endregion

        #region ' публичные методы для предоставления данных о блоке '
        public int X { get { return startPoint.X; } }

        public int Y { get { return startPoint.Y; } }

        public int Width { get { return blockSize.Width; } }

        public void SetWidth(int newWidth)
        {
            blockSize.Width = newWidth;
            // корректируем отображение связей
            foreach (BlocksLink lnk in Links)
                lnk.FindOptimalPoints();
        }

        public void SetY(int newY)
        {
            startPoint.Y = newY;
            // корректируем отображение связей
            foreach (BlocksLink lnk in Links)
                lnk.FindOptimalPoints();
        }

        public void SetX(int newX)
        {
            startPoint.X = newX;
            // корректируем отображение связей
            foreach (BlocksLink lnk in Links)
                lnk.FindOptimalPoints();
        }

        public int Height { get { return blockSize.Height; } }

        //public int Right { get { return X + Width; } }
        //public int Bottom { get { return Y + Height; } }

        public Rectangle Rect { get { return new Rectangle(X, Y, Width, Height); } }

        //public BlockType BlockType { get { return blockType; } }

        public Size Size { get { return blockSize; } }


        #endregion

        #region ' работа с ссылками'

        /// <summary>
        /// проверка на вставку связи
        /// </summary>
        /// <param name="startLink"></param>
        /// <returns></returns>
        public string CanAddLink(bool startLink)
        {
            string answer = "";
            if (IsLine || IsTimeLine)
                return null;
            if (!infinityLinks)
            {
                int maxlnk;
                if (startLink)
                {
                    maxlnk = getMaxPossibleOutLinks();
                    if (maxlnk > -1)
                    {
                        if (outLinks.Count >= maxlnk) { answer = " У этого блока не может быть больше исходящих связей."; }
                    }
                }
                else
                {
                    maxlnk = getMaxPossibleInLinks();
                    if (maxlnk > -1)
                    {
                        if (inLinks.Count >= maxlnk) { answer = " У этого блока не может быть больше входящих связей."; }
                    }
                }
            }
            return answer;
        }

        /// <summary>
        /// регистрация связи с другим блоком
        /// </summary>
        public void RegisterLink(BlocksLink link)
        {
            // Если такая связь не зарегистрирована - регистрируем
            if (!Links.Contains(link))
            {
                Links.Add(link);
                if (link.First == this)
                {
                    outLinks.Add(link);
                }
                else
                {
                    inLinks.Add(link);
                }
            }
        }

        /// <summary>
        /// удаление одной ссылки (без проверки этой ссылки)
        /// </summary>
        public void RemoveLink(BlocksLink lnk)
        {
            if (Links.Contains(lnk)) { Links.Remove(lnk); }
            if (outLinks.Contains(lnk)) { outLinks.Remove(lnk); }
            if (inLinks.Contains(lnk)) { inLinks.Remove(lnk); }
        }

        /// <summary>
        /// удаление всех ссылок
        /// </summary> 
        public void RemoveLinks()
        {
            foreach (BlocksLink lnk in Links)
            {
                lnk.Remove(this);
            }
            Links.Clear();
            outLinks.Clear();
            inLinks.Clear();
        }
        #endregion

        #region ' функции отвечающие за изменения размера и местоположения'
        /// <summary>
        /// корректируем расположение (используется при перемещении)
        /// </summary>
        public void PositionCorrection(Point p, Size mvBlockPointDelta)
        {

            // корректируем позицию самого блока
            if (blockType != BlockType.Line)
                startPoint.X = p.X - mvBlockPointDelta.Width;
            startPoint.Y = p.Y - mvBlockPointDelta.Height;
            // и его вложенных блоков
            foreach (BlockObject child in children)
            {
                child.startPoint.Y = Y + 1;
                foreach (BlocksLink lnk in child.Links)
                    lnk.FindOptimalPoints();
            }
            // корректируем отображение связей
            foreach (BlocksLink lnk in Links)
                lnk.FindOptimalPoints();
        }

        /// <summary>
        /// корректируем размер
        /// </summary>
        public void SizeCorrection(Point p, Point PreviousPoint, Size PreviousSize)
        {
            // если сюжлиния то ширину не трогаем
            if (blockType != BlockType.Line)
                blockSize.Width = PreviousSize.Width + (p.X - PreviousPoint.X);
            if (Options.DoBlockHeightResize)
                blockSize.Height = PreviousSize.Height + (p.Y - PreviousPoint.Y);

            // корректируем отображение связей
            foreach (BlocksLink lnk in Links)
            {
                lnk.FindOptimalPoints();
            }
        }

        /// <summary>
        /// завершаем перемещение или изменение размера
        /// </summary> 
        public void CompleteCorrection()
        {
            blockSideCorrection();
        }

        /// <summary>
        /// установка стартовой точки в верхний левый угол блока, где бы она не была
        /// необходимость в этом появляется если блок рисуется ведением мыши снизу-вверх и/или справа-налево
        /// </summary> 
        private void blockSideCorrection()
        {
            if (blockSize.Width < 0)
            {
                startPoint.X = startPoint.X + blockSize.Width;
                blockSize.Width = 0 - blockSize.Width;
            }
            if (blockSize.Height < 0)
            {
                startPoint.Y = startPoint.Y + blockSize.Height;
                blockSize.Height = 0 - blockSize.Height;
            }
        }
        #endregion

        #region вспомогательные методы

        /// <summary>
        /// проверяем, находится ли переданная точка в нашем блоке
        /// </summary>
        public bool isInside(Point cp)
        {
            bool answer = false;
            int xs, xe;
            int ys, ye;

            // после появления blockSideCorrection уже неактуально, но пока убирать не буду
            if (blockSize.Width < 0) { xs = startPoint.X + blockSize.Width; xe = startPoint.X; }
            else { xs = startPoint.X; xe = startPoint.X + blockSize.Width; }

            if (blockSize.Width < 0) { ys = startPoint.Y + blockSize.Height; ye = startPoint.Y; }
            else { ys = startPoint.Y; ye = startPoint.Y + blockSize.Height; }

            if ((xs < cp.X) && (cp.X < xe)) if ((ys < cp.Y) && (cp.Y < ye)) answer = true;

            return answer;
        }

        /// <summary>
        /// проверяем, находится ли переданный блок в нашем блоке
        /// </summary>
        public bool isInside(BlockObject cb)
        {
            return (startPoint.X < cb.X) && (startPoint.Y < cb.Y) &&
                ((blockSize.Width - (cb.X - startPoint.X) > cb.Width) && (blockSize.Height - (cb.Y - startPoint.Y) > cb.Height));
        }

        /// <summary>
        /// проверяем, находится ли переданная точка на маркерах и возвращаем требуемый курсор
        /// </summary>
        public bool isInsideResArea(Point cp)
        {
            bool answer = false;
            Point whereDraw = new Point();
            whereDraw.X = startPoint.X + blockSize.Width;
            whereDraw.Y = startPoint.Y + blockSize.Height;
            if ((cp.X > (whereDraw.X - resArea)) && (cp.X < whereDraw.X))
            {
                if ((cp.Y > (whereDraw.Y - resArea)) && (cp.Y < whereDraw.Y))
                {
                    answer = true;
                }
            }
            return answer;
        }
        #endregion

        #region обсчёты данных

        // установка данных
        public void SetDataValue(int dtype)
        {
            dataType = dtype;
            switch (dtype)
            {
                case 1:
                    dataValue = "0"; break;
                default:
                    dataValue = "0"; break;
            }
            if (linkLimitOut())
            {
                RemoveLinks();
            }
        }
        public void SetDataValue(string dvalue)
        {
            dataType = 1;
            dataValue = dvalue;
        }

        public int GetDataType()
        {
            return dataType;
        }

        private int getMaxPossibleOutLinks()
        {
            int answer = -1;
            switch (dataType)
            {
                // starting block
                //case 0:
                //    answer = 1; break;
                default:
                    break;
            }
            return answer;
        }
        private int getMaxPossibleInLinks()
        {
            int answer = -1;
            switch (dataType)
            {
                // starting block
                //case 0:
                //    answer = 0; break;
                default:
                    break;
            }
            return answer;
        }

        private bool linkLimitOut()
        {
            int maxlnk;
            maxlnk = getMaxPossibleOutLinks();
            if (maxlnk > -1)
            {
                if (outLinks.Count > maxlnk) { return true; }
            }
            maxlnk = getMaxPossibleInLinks();
            if (maxlnk > -1)
            {
                if (inLinks.Count > maxlnk) { return true; }
            }
            return false;
        }

        
        #endregion

        #region ' рисование'
        /// <summary>
        /// рисуем блок
        /// </summary>        
        public void DrawIt(Graphics g, PictureBox picBox)
        {
            g = Graphics.FromImage(picBox.Image);
            // для каждого блока своё рисование
            int dataTxtWidth;
            switch (blockType)
            {
                case BlockType.Rect:
                    g.DrawRectangle(blockPen, startPoint.X, startPoint.Y, blockSize.Width, blockSize.Height);
                    break;
                case BlockType.Timeline:
                    g.DrawRectangle(blockPen, startPoint.X, startPoint.Y, blockSize.Width, blockSize.Height);
                    TimeLine tl = TimeLine.Line;
                    if (tl.IdTimepart.HasValue && tl.IntervalValue.HasValue && tl.DateEnd.HasValue && tl.DateStart.HasValue)
                    {
                        TimeSpan time = tl.DateEnd.Value - tl.DateStart.Value;
                        double sectionval = 0;
                        string ticktitle = TimePart.GetTimeParts().OrderBy(x => x.Id).ToList()[tl.IdTimepart.Value].Title;
                        int section = 50;
                        int mult = 1;
                        switch (tl.IdTimepart.Value)
                        {
                            case 0:
                                section = (int)Math.Round(tl.Len * tl.IntervalValue.Value / time.TotalSeconds);
                                if (section > tl.Len / 2 || section < 50)
                                    section = 100;
                                sectionval = time.TotalSeconds * section / tl.Len;
                                mult = 1;
                                break;
                            case 1:
                                section = (int)Math.Round(tl.Len * tl.IntervalValue.Value / time.TotalMinutes);
                                if (section > tl.Len / 2 || section < 50)
                                    section = 100;
                                sectionval = time.TotalMinutes * section / tl.Len;
                                mult = 60;
                                break;
                            case 2:
                                section = (int)Math.Round(tl.Len * tl.IntervalValue.Value / time.TotalHours);
                                if (section > tl.Len / 2 || section < 50)
                                    section = 100;
                                sectionval = time.TotalHours * section / tl.Len;
                                mult = 3600;
                                break;
                            case 3:
                                section = (int)Math.Round(tl.Len * tl.IntervalValue.Value / time.TotalDays);
                                if (section > tl.Len / 2 || section < 50)
                                    section = 100;
                                sectionval = time.TotalDays * section / tl.Len;
                                mult = 86400;
                                break;
                            case 4:
                                section = (int)Math.Round(tl.Len * tl.IntervalValue.Value / time.TotalDays / 7);
                                if (section > tl.Len / 2 || section < 50)
                                    section = 100;
                                sectionval = time.TotalDays * section / tl.Len / 7;
                                mult = 604800;
                                break;
                            case 5:
                                section = (int)Math.Round(tl.Len * tl.IntervalValue.Value / time.TotalDays / 31);
                                if (section > tl.Len / 2 || section < 50)
                                    section = 100;
                                sectionval = time.TotalDays * section / tl.Len / 31;
                                mult = 2678400;
                                break;
                            case 6:
                                section = (int)Math.Round(tl.Len * tl.IntervalValue.Value / time.TotalDays / 365);
                                if (section > tl.Len / 2 || section < 50)
                                    section = 100;
                                sectionval = time.TotalDays * section / tl.Len / 365;
                                mult = 34536000;
                                break;
                            case 7:
                                break;
                            case 8:
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                        int pix = 0;
                        double val = 0;
                        string txt = "";
                        while (pix < tl.Len)
                        {
                            if (pix == 0)
                                txt = tl.DateStart.Value.ToString();
                            else
                                if (Options.RelativeTimelineSections)
                                {
                                    TimeSpan span = new TimeSpan((int)Math.Round(val * mult / 3600),
                                        (int)Math.Round(val * mult % 3600 / 60), (int)Math.Round(val * mult % 3600 % 60 / 60));
                                    string format = "";
                                    switch (tl.IdTimepart.Value)
                                    {
                                        case 0:
                                            format = "HH:mm:ss";
                                            break;
                                        case 1:
                                        case 2:
                                            format = "HH:mm";
                                            break;
                                        case 3:
                                        case 4:
                                        case 5:
                                        case 6:
                                            format = "dd.MM.yyyy";
                                            break;
                                        default:
                                            break;
                                    }
                                    txt = (tl.DateStart.Value + span).ToString(format);
                                }
                                else
                                    txt = ((int)Math.Round(val)).ToString() + ticktitle;
                            dataTxtWidth = txt.Length * Consts.DATA_CHAR_WIDTH;

                            g.DrawLine(blockPen, pix, startPoint.Y + Consts.DATA_CHAR_HEIGHT, pix, startPoint.Y + blockSize.Height - Consts.DATA_CHAR_HEIGHT);
                            g.DrawString(txt, new Font(Consts.DATA_FONT_NAME, Consts.DATA_FONT_SIZE), blockPen.Brush,
                                pix - (pix == 0 ? 0 : dataTxtWidth / 2), startPoint.Y + (pix == 0 ? 0 : blockSize.Height - Consts.DATA_CHAR_HEIGHT));
                            val += sectionval;
                            pix += section;
                        }
                        txt = tl.DateEnd.Value.ToString();
                        dataTxtWidth = txt.Length * Consts.DATA_CHAR_WIDTH;
                        g.DrawString(txt, new Font(Consts.DATA_FONT_NAME, Consts.DATA_FONT_SIZE), blockPen.Brush,
                                blockSize.Width - dataTxtWidth, startPoint.Y);
                    }
                    break;
                case BlockType.Line:
                    g.DrawRectangle(blockPen, startPoint.X, startPoint.Y, blockSize.Width, blockSize.Height);
                    int half = startPoint.Y + blockSize.Height / 2;
                    // для сюжлинии делаем пробелы там, где вложенные блоки
                    Region reg = new Region(new Rectangle(0, 0, blockSize.Width, 1));
                    foreach (BlockObject child in children)
                        reg.Exclude(new Rectangle(child.X, 0, child.Width, 1));
                    foreach (RectangleF line in reg.GetRegionScans(new System.Drawing.Drawing2D.Matrix()))
                        g.DrawLine(Consts.pen, line.Left, half, line.Right, half);
                    break;
                case BlockType.Elps:
                    g.DrawEllipse(blockPen, startPoint.X, startPoint.Y, blockSize.Width, blockSize.Height);
                    break;
                case BlockType.Romb:
                    Point[] points = 
                    {
                        new Point(startPoint.X, startPoint.Y + blockSize.Height / 2),
                        new Point(startPoint.X + blockSize.Width / 2, startPoint.Y),
                        new Point(startPoint.X + blockSize.Width, startPoint.Y + blockSize.Height / 2),
                        new Point(startPoint.X + blockSize.Width / 2, startPoint.Y + blockSize.Height)
                    };
                    g.DrawPolygon(blockPen, points);
                    break;
                case BlockType.Hex:
                    points = new Point[]
                    {
                        new Point(startPoint.X, startPoint.Y + blockSize.Height / 2),
                        new Point(startPoint.X + blockSize.Width / 4, startPoint.Y),
                        new Point(startPoint.X + 3 * blockSize.Width / 4, startPoint.Y),
                        new Point(startPoint.X + blockSize.Width, startPoint.Y + blockSize.Height / 2),
                        new Point(startPoint.X + 3 * blockSize.Width / 4, startPoint.Y + blockSize.Height),
                        new Point(startPoint.X + blockSize.Width / 4, startPoint.Y + blockSize.Height)
                    };
                    g.DrawPolygon(blockPen, points);
                    break;
                default:
                    break;
            }

            // рисование текста
            dataTxtWidth = dataValue.Length * Consts.DATA_CHAR_WIDTH;
            // у линии в начале вписываем текст в рект
            if (IsLine)
                g.DrawString(dataValue, new Font(Consts.DATA_FONT_NAME, Consts.DATA_FONT_SIZE), blockPen.Brush,
                    new Rectangle(Consts.LineTextZone.Left, startPoint.Y + Consts.LineTextZone.Top,
                        Consts.LineTextZone.Right, startPoint.Y + Math.Min(Consts.LineTextZone.Top, blockSize.Height)));
            else
                // у блока просто в центре текст
                g.DrawString(dataValue, new Font(Consts.DATA_FONT_NAME, Consts.DATA_FONT_SIZE), blockPen.Brush,
                    startPoint.X + blockSize.Width / 2 - dataTxtWidth / 2, startPoint.Y + blockSize.Height / 2 - Consts.DATA_CHAR_HEIGHT / 2);

            if (Selected) { drawSelectedStigmates(g); }
            g.Dispose();
        }

        /// <summary>
        /// рисуем блок с штихованой областью изменения размера, если надо
        /// </summary>
        public void DrawIt(Graphics g, PictureBox picBox, bool withResizeArea)
        {
            DrawIt(g, picBox);
            if (withResizeArea) { drawResArea(g, picBox); }
        }

        private void drawStigmate(Graphics g, Point whereDraw)
        {
            whereDraw.X = whereDraw.X - (stigSize / 2);
            whereDraw.Y = whereDraw.Y - (stigSize / 2);
            g.DrawEllipse(blockPen, whereDraw.X, whereDraw.Y, 4, 4);
        }

        private void drawResArea(Graphics g, PictureBox picBox)
        {
            g = Graphics.FromImage(picBox.Image);
            Point eP = new Point(startPoint.X + blockSize.Width, startPoint.Y + blockSize.Height);
            for (int i = resAreaLineDelta; i <= resArea; i += resAreaLineDelta)
            {
                g.DrawLine(blockPen, new Point(eP.X - i, eP.Y), new Point(eP.X, eP.Y - i));
            }
            g.Dispose();
        }

        /// <summary>
        /// рисует обозначение выделенности
        /// </summary>
        private void drawSelectedStigmates(Graphics g)
        {
            Point whereDraw = new Point();
            whereDraw.X = startPoint.X;
            whereDraw.Y = startPoint.Y;
            drawStigmate(g, whereDraw);
            whereDraw.X = startPoint.X + (blockSize.Width / 2);
            whereDraw.Y = startPoint.Y;
            drawStigmate(g, whereDraw);
            whereDraw.X = startPoint.X + blockSize.Width;
            whereDraw.Y = startPoint.Y;
            drawStigmate(g, whereDraw);
            whereDraw.X = startPoint.X;
            whereDraw.Y = startPoint.Y + (blockSize.Height / 2);
            drawStigmate(g, whereDraw);
            whereDraw.X = startPoint.X + blockSize.Width;
            whereDraw.Y = startPoint.Y + (blockSize.Height / 2);
            drawStigmate(g, whereDraw);
            whereDraw.X = startPoint.X;
            whereDraw.Y = startPoint.Y + blockSize.Height;
            drawStigmate(g, whereDraw);
            whereDraw.X = startPoint.X + (blockSize.Width / 2);
            whereDraw.Y = startPoint.Y + blockSize.Height;
            drawStigmate(g, whereDraw);
            whereDraw.X = startPoint.X + blockSize.Width;
            whereDraw.Y = startPoint.Y + blockSize.Height;
            drawStigmate(g, whereDraw);
        }
        #endregion
    }
}
