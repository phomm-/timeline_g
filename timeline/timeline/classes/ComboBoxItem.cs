﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Timeline
{

    /// <summary>
    /// Комбобокс айтем
    /// </summary>
    public class ComboBoxItem
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        public ComboBoxItem(int Id, string Name)
        {
            this.Id = Id;
            this.Name = Name;
        }

        /// <summary>
        /// Перегрузка
        /// </summary>
        public override string ToString()
        {
            return this.Name;
        }

    }

}
