﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Xml;

namespace Timeline
{

    /// <summary>
    /// События
    /// </summary>
    public class Event
    {

        #region Свойства

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// XML-узел
        /// </summary>
        private XmlNode _Node { get; set; }
        private TimeLine Timeline { get; set; }

        /// <summary>
        /// Навание
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Старт дата
        /// </summary>
        public DateTime? DateStart { get; set; }

        /// <summary>
        /// Конец дата
        /// </summary>
        public DateTime? DateEnd { get; set; }

        /// <summary>
        /// Информация
        /// </summary>
        public string Memo { get; set; }

        public int vis_top { get; set; }
        public int vis_left { get; set; }
        public int vis_width { get; set; }
        public int vis_height { get; set; }
        public int line_width { get; set; }
        public int shape { get; set; }
        public int color { get; set; }

        public int IdTimeline { get; set; }
        public int? IdStoryline { get; set; }
        public BlockType Blocktype
        {
            get
            {
                switch (shape)
                {
                    case 0:
                        return BlockType.Hex;
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        public Pen Pen { get { return new Pen(Color.FromArgb((byte)color, (byte)(color >> 8), (byte)(color >> 16)), line_width); } }
        public Point StartPoint { get { return new Point(vis_left, vis_top); } }
        public Size Size { get { return new Size(vis_width, vis_height); } }

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        public Event(int Id, int IdTimeline)
        {
            this.Id = Id;
            Timeline = TimeLine.GetTimeLines().First(x => x.Id == IdTimeline);
            if (Timeline._Node["Events"] == null)
                Timeline._Node.AppendChild(Timeline._Doc.CreateElement("Events"));
            this._Node = Timeline._Node["Events"].AppendChild(Timeline._Doc.CreateElement("Event"));
        }

        /// <summary>
        /// Конструктор по XML-узлу
        /// </summary>
        public Event(XmlNode Node)
        {
            this._Node = Node;
            this.Id = Convert.ToInt32(this._Node["Id"].InnerText);
            this.Loading();
        }

        #endregion

        #region Методы

        static public IEnumerable<Event> GetEvents(int idTimeline)
        {
            List<Event> result = new List<Event>();
            TimeLine tl = TimeLine.GetTimeLines().First(x => x.Id == idTimeline);
            if (tl._Node["Events"] != null && tl._Node["Events"].HasChildNodes)
                foreach (XmlElement el in tl._Node["Events"])
                {
                    Event ev = new Event(el);
                    ev.Timeline = tl;
                    result.Add(ev);
                }
            return result;
        }

        /// <summary>
        /// Загрузить
        /// </summary>
        private void Loading()
        {
            this.Name = Convert.ToString(this._Node["Name"].InnerText);
            this.Memo = Convert.ToString(this._Node["Memo"].InnerText);
            this.vis_left = Convert.ToInt32(this._Node["vis_left"].InnerText);
            this.vis_top = Convert.ToInt32(this._Node["vis_top"].InnerText);
            this.vis_width = Convert.ToInt32(this._Node["vis_width"].InnerText);
            this.vis_height = Convert.ToInt32(this._Node["vis_height"].InnerText);
            this.line_width = Convert.ToInt32(this._Node["line_width"].InnerText);
            this.color = Convert.ToInt32(this._Node["color"].InnerText);
            this.DateStart = TimeLine.CheckedDT(_Node["DateStart"]);
            this.DateEnd = TimeLine.CheckedDT(_Node["DateEnd"]);
            this.IdTimeline = Convert.ToInt32(this._Node["IdTimeline"].InnerText);
            this.IdStoryline = TimeLine.CheckedInt(_Node["IdStoryline"]);
        }

        public void Modify(int left, int top, int width, int height, int color, int lineWidth, int? story)
        {
            vis_left = left;
            vis_top = top;
            vis_height = height;
            vis_width = width;
            this.color = color;
            line_width = lineWidth;
            IdStoryline = story;
        }

        public void ModifySpecs(string name, string memo, DateTime? start, DateTime? end)
        {
            Name = name;
            Memo = memo;
            DateStart = start;
            DateEnd = end;
        }

        /// <summary>
        /// Обновить
        /// </summary>
        public void Update()
        {
            Timeline.ModifyNode(_Node, "Name", this.Name);
            Timeline.ModifyNode(_Node, "Memo", this.Memo);
            Timeline.ModifyNode(_Node, "IdTimeline", this.IdTimeline.ToString());
            UpdateStoryLine(IdStoryline);
            Timeline.ModifyNode(_Node, "vis_top", this.vis_top.ToString());
            Timeline.ModifyNode(_Node, "vis_left", this.vis_left.ToString());
            Timeline.ModifyNode(_Node, "vis_width", this.vis_width.ToString());
            Timeline.ModifyNode(_Node, "vis_height", this.vis_height.ToString());
            Timeline.ModifyNode(_Node, "line_width", this.line_width.ToString());
            Timeline.ModifyNode(_Node, "color", this.color.ToString());
            Timeline.ModifyNode(_Node, "DateStart", this.DateStart.ToString());
            Timeline.ModifyNode(_Node, "DateEnd", this.DateEnd.ToString());
            Timeline.ModifyNode(_Node, "Id", this.Id.ToString());
        }

        internal void UpdateStoryLine(int? story)
        {
            IdStoryline = story;
            Timeline.ModifyNode(_Node, "IdStoryline", this.IdStoryline.ToString());
        }

        /// <summary>
        /// Удалить
        /// </summary>
        public static void Delete(int Id)
        {
            Event ev = GetEvents(TimeLine.Line.Id).First(x => x.Id == Id);
            ev._Node.ParentNode.RemoveChild(ev._Node);
        }

        /// <summary>
        /// Создать
        /// </summary>
        public static Event Create(string name, int idTimeline)
        {
            int id = 0;
            foreach (TimeLine line in TimeLine.GetTimeLines())
            
            {
                var events = GetEvents(line.Id);
                if (events.Count() > 0)
                    id = Math.Max(id, events.Max(e => e.Id));
            }
            Event ev = new Event(++id, idTimeline);
            ev.Name = name;
            ev.IdTimeline = idTimeline;
            ev.Update();
            return ev;
        }

        #endregion

    }
}
