﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Xml;

namespace Timeline
{
    class Storyline
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; private set; }

        private TimeLine Timeline { get; set; }

        /// <summary>
        /// XML-узел
        /// </summary>
        private XmlNode _Node { get; set; }
        //internal XmlDocument _Doc { get; set; }
        
        /// <summary>
        /// Навание
        /// </summary>
        public string Name { get; set; }
        public string Memo { get; set; }
        public string FullName { get; set; }
        public string StorylineType { get; set; }
        public string Notes { get; set; }
        public int color { get; set; }
        public int len { get; set; }
        public int line_width { get; set; }
        public int vis_top { get; set; }

        public int vis_height { get; set; }

        public Pen Pen { get { return new Pen(Color.FromArgb((byte)color, (byte)(color >> 8), (byte)(color >> 16)), line_width); } }
        public Point StartPoint { get { return new Point(0, vis_top); } }
        public Size Size { get { return new Size(len, vis_height); } }

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        public Storyline(int Id, int idTimeline)
        {
            this.Id = Id;
            Timeline = TimeLine.GetTimeLines().First(x => x.Id == idTimeline);
            if (Timeline._Node["Storylines"] == null)
                Timeline._Node.AppendChild(Timeline._Doc.CreateElement("Storylines"));
            this._Node = Timeline._Node["Storylines"].AppendChild(Timeline._Doc.CreateElement("Storyline"));
         }

        /// <summary>
        /// Конструктор по XML-узлу
        /// </summary>
        public Storyline(XmlNode Node)
        {
            this._Node = Node;
            this.Id = Convert.ToInt32(this._Node["Id"].InnerText);
            this.Loading();
        }

        #endregion

        #region Методы

        static public IEnumerable<Storyline> GetStorylines(int idTimeline)
        {
            List<Storyline> result = new List<Storyline>();
            TimeLine tl = TimeLine.GetTimeLines().First(x => x.Id == idTimeline);
            if (tl._Node["Storylines"] != null && tl._Node["Storylines"].HasChildNodes)
                foreach (XmlElement el in tl._Node["Storylines"])
                {
                    Storyline sl = new Storyline(el);
                    sl.Timeline = tl;
                    result.Add(sl);
                }
            return result;
        }

        public void Modify(int top, int height, int color, int lineWidth, int len)
        {
            vis_top = top;
            vis_height = height;
            line_width = lineWidth;
            this.len = len;
        }

        public void ModifySpecs(string name, string memo, string fullName, string storylineType, string notes)
        {
            Name = name;
            Memo = memo;
            FullName = fullName;
            StorylineType = storylineType;
            Notes = notes;
        }

        /// <summary>
        /// Обновить
        /// </summary>
        public void Update()
        {
            Timeline.ModifyNode(_Node, "Name", this.Name);
            Timeline.ModifyNode(_Node, "Memo", this.Memo);
            Timeline.ModifyNode(_Node, "FullName", this.FullName);
            Timeline.ModifyNode(_Node, "Notes", this.Notes);
            Timeline.ModifyNode(_Node, "StorylineType", this.StorylineType);
            Timeline.ModifyNode(_Node, "vis_top", this.vis_top.ToString());
            Timeline.ModifyNode(_Node, "vis_height", this.vis_height.ToString());
            Timeline.ModifyNode(_Node, "line_width", this.line_width.ToString());
            Timeline.ModifyNode(_Node, "len", this.len.ToString());
            Timeline.ModifyNode(_Node, "color", this.color.ToString());
            Timeline.ModifyNode(_Node, "Id", this.Id.ToString());
        }

        /// <summary>
        /// Загрузить
        /// </summary>
        private void Loading()
        {
            this.Name = Convert.ToString(this._Node["Name"].InnerText);
            this.Memo = Convert.ToString(this._Node["Memo"].InnerText);
            this.FullName = Convert.ToString(this._Node["FullName"].InnerText);
            this.StorylineType = Convert.ToString(this._Node["StorylineType"].InnerText);
            this.Notes = Convert.ToString(this._Node["Notes"].InnerText);
            this.vis_top = Convert.ToInt32(this._Node["vis_top"].InnerText);
            this.vis_height = Convert.ToInt32(this._Node["vis_height"].InnerText);
            this.color = Convert.ToInt32(this._Node["color"].InnerText);
            this.len = Convert.ToInt32(this._Node["len"].InnerText);
            this.line_width = Convert.ToInt32(this._Node["line_width"].InnerText);
        }

        
        /// <summary>
        /// Удалить
        /// </summary>
        public static void Delete(int Id)
        {
            Storyline storyline = GetStorylines(TimeLine.Line.Id).First(x => x.Id == Id);
            foreach (Event ev in Event.GetEvents(TimeLine.Line.Id).Where(x => x.IdStoryline == Id))
                ev.UpdateStoryLine(null);
            storyline._Node.ParentNode.RemoveChild(storyline._Node);
        }

        /// <summary>
        /// Создать
        /// </summary>
        public static Storyline Create(string name, int idTimeline)
        {
            int id = 1;
            if (GetStorylines(idTimeline).Count() != 0)
                id = GetStorylines(idTimeline).Max(x => x.Id) + 1;
            Storyline sl = new Storyline(id, idTimeline);
            sl.Name = name;            
            sl.Update();
            return sl;
        }

        #endregion
    }
}
