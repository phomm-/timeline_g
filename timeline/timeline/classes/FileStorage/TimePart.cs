﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Timeline
{
    class TimePart
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// XML-узел
        /// </summary>
        private XmlNode _Node { get; set; }
        
        /// <summary>
        /// Навание
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Подпись шкалы
        /// </summary>
        public string Title { get; set; }

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        public TimePart(int Id)
        {
            this.Id = Id;
            if (Loader._Doc.DocumentElement["TimeParts"] == null)
                Loader._Doc.DocumentElement.AppendChild(Loader._Doc.CreateElement("TimeParts"));
            this._Node = Loader._Doc.DocumentElement["TimeParts"].AppendChild(Loader._Doc.CreateElement("TimePart"));
        }

        /// <summary>
        /// Конструктор по XML-узлу
        /// </summary>
        public TimePart(XmlNode Node, bool fully = true)
        {
            this._Node = Node;
            this.Id = Convert.ToInt32(this._Node["Id"].InnerText);
            this.Loading();
        }

        #endregion

        #region Методы

        static public IEnumerable<TimePart> GetTimeParts()
        {
            List<TimePart> result = new List<TimePart>();
            if (Loader._Doc.DocumentElement["TimeParts"] != null)
                foreach (XmlNode node in Loader._Doc.DocumentElement["TimeParts"])
                    result.Add(new TimePart(node));
            return result;
        }

        /// <summary>
        /// Загрузить
        /// </summary>
        private void Loading()
        {
            this.Name = Convert.ToString(this._Node["Name"].InnerText);
            this.Title = Convert.ToString(this._Node["Title"].InnerText);
        }


        #endregion
    }
}
