﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Timeline
{
    public class Person
    {

        #region Свойства

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// XML-узел
        /// </summary>
        private XmlNode _Node { get; set; }
        
        public string Name { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Gender { get; set; }
        public string PersonType { get; set; }
        public string Notes { get; set; }
        public DateTime? Birth { get; set; }
        public DateTime? Death { get; set; }

        /// <summary>
        /// Информация
        /// </summary>
        public string Memo { get; set; }

        private HashSet<int> Events = new HashSet<int>();

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        public Person(int Id)
        {
            this.Id = Id;
            if (Loader._Doc.DocumentElement["Persons"] == null)
                Loader._Doc.DocumentElement.AppendChild(Loader._Doc.CreateElement("Persons"));
            this._Node = Loader._Doc.DocumentElement["Persons"].AppendChild(Loader._Doc.CreateElement("Person"));
        }

        /// <summary>
        /// Конструктор по XML-узлу
        /// </summary>
        public Person(XmlNode Node, bool fully = true)
        {
            this._Node = Node;
            this.Id = Convert.ToInt32(this._Node["Id"].InnerText);
            this.Loading();
        }

        #endregion

        #region Методы

        static public IEnumerable<Person> GetPersons()
        {
            List<Person> result = new List<Person>();
            if (Loader._Doc.DocumentElement["Persons"] != null)
                foreach (XmlNode node in Loader._Doc.DocumentElement["Persons"])
                {
                    Person a = new Person(node);
                    result.Add(a);
                    foreach (XmlNode enode in node["Events"])
                        a.Events.Add(Convert.ToInt32(enode.InnerText));
                }
            return result;
        }

        /// <summary>
        /// Загрузить
        /// </summary>
        private void Loading()
        {

            this.Name = Convert.ToString(this._Node["Name"].InnerText);
            this.Memo = Convert.ToString(this._Node["Memo"].InnerText);
            this.LastName = Convert.ToString(this._Node["LastName"].InnerText);
            this.FirstName = Convert.ToString(this._Node["FirstName"].InnerText);
            this.MiddleName = Convert.ToString(this._Node["MiddleName"].InnerText);
            this.Gender = Convert.ToString(this._Node["Gender"].InnerText);
            this.PersonType = Convert.ToString(this._Node["PersonType"].InnerText);
            this.Notes = Convert.ToString(this._Node["Notes"].InnerText);
            this.Birth = TimeLine.CheckedDT(_Node["Birth"]);
            this.Death = TimeLine.CheckedDT(_Node["Death"]); 
        }


        public bool IsEventPerson(int idEvent)
        {
            return Events.Contains(idEvent);
        }

        /// <summary>
        /// Обновить
        /// </summary>
        public void Update()
        {
            Loader.ModifyNode(_Node, "Name", this.Name);
            Loader.ModifyNode(_Node, "Memo", this.Memo);
            Loader.ModifyNode(_Node, "LastName", this.LastName);
            Loader.ModifyNode(_Node, "FirstName", this.FirstName);
            Loader.ModifyNode(_Node, "MiddleName", this.MiddleName);
            Loader.ModifyNode(_Node, "Gender", this.Gender);
            Loader.ModifyNode(_Node, "PersonType", this.PersonType);
            Loader.ModifyNode(_Node, "Notes", this.Notes);
            Loader.ModifyNode(_Node, "Birth", this.Birth.ToString());
            Loader.ModifyNode(_Node, "Death", this.Death.ToString());
            Loader.ModifyNode(_Node, "Id", this.Id.ToString());
            UpdateEvents();
        }

        private void UpdateEvents()
        {
            if (_Node["Events"] == null)
                Loader.ModifyNode(_Node, "Events", null);
            _Node["Events"].RemoveAll();
            foreach (int i in Events)
            {
                XmlNode ev = Loader._Doc.CreateElement("IdEvent");
                ev.InnerText = i.ToString();
                _Node["Events"].AppendChild(ev);
            }
        }

        /// <summary>
        /// Обновить событие
        /// </summary>
        public void AlterEvent(int idEvent, bool doBind)
        {
            if (doBind)
                Events.Add(idEvent);
            else
                Events.Remove(idEvent);
            UpdateEvents();
        }

        public void AlterPersonRole(int idEvent, string role)
        {

        }

        public string GetPersonRole(int idEvent)
        {
            return "";
        }

        /// <summary>
        /// Удалить
        /// </summary>
        public static void Delete(int Id)
        {
            Person person = GetPersons().First(x => x.Id == Id);
            person._Node.ParentNode.RemoveChild(person._Node);
        }

        /// <summary>
        /// Создать
        /// </summary>
        public static Person Create(string name)
        {
            int id = 1;
            if (GetPersons().Count() != 0)
                id = GetPersons().Max(x => x.Id) + 1;
            Person p = new Person(id);
            p.Name = name;
            p.Update();
            return p;    
        }

        #endregion

    }
}
