﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace Timeline
{

    /// <summary>
    /// Территории
    /// </summary>
    public class Area
    {


        #region Свойства

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// XML-узел
        /// </summary>
        private XmlNode _Node { get; set; }

        
        
        /// <summary>
        /// Навание
        /// </summary>
        public string Name { get; set; }

        public string Country { get; set; }
        public string City { get; set; }
        public string Place { get; set; }

        public string Notes { get; set; }


        /// <summary>
        /// Информация
        /// </summary>
        public string Memo { get; set; }

        private HashSet<int> Events = new HashSet<int>();

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        public Area(int Id)
        {
            this.Id = Id;
            if (Loader._Doc.DocumentElement["Areas"] == null)
                Loader._Doc.DocumentElement.AppendChild(Loader._Doc.CreateElement("Areas"));
            this._Node = Loader._Doc.DocumentElement["Areas"].AppendChild(Loader._Doc.CreateElement("Area"));
        }

        /// <summary>
        /// Конструктор по XML-узлу
        /// </summary>
        public Area(XmlNode Node)
        {
            this._Node = Node;
            this.Id = Convert.ToInt32(this._Node["Id"].InnerText);
            this.Loading();
        }

        #endregion

        #region Методы

        static public IEnumerable<Area> GetAreas()
        {
            List<Area> result = new List<Area>();
            if (Loader._Doc.DocumentElement["Areas"] != null)
                foreach (XmlNode node in Loader._Doc.DocumentElement["Areas"])
                {
                    Area a = new Area(node);
                    result.Add(a);
                    foreach (XmlNode enode in node["Events"])
                        a.Events.Add(Convert.ToInt32(enode.InnerText));
                }
            return result;
        }

        /// <summary>
        /// Загрузить
        /// </summary>
        private void Loading()
        {

            this.Name = Convert.ToString(this._Node["Name"].InnerText);
            this.Memo = Convert.ToString(this._Node["Memo"].InnerText);
            this.Country = Convert.ToString(this._Node["Country"].InnerText);
            this.City = Convert.ToString(this._Node["City"].InnerText);
            this.Place = Convert.ToString(this._Node["Place"].InnerText);
            this.Notes = Convert.ToString(this._Node["Notes"].InnerText);
        }


        public bool IsEventArea(int idEvent)
        {
            return Events.Contains(idEvent);
        }

        /// <summary>
        /// Обновить
        /// </summary>
        public void Update()
        {
            Loader.ModifyNode(_Node, "Name", this.Name);
            Loader.ModifyNode(_Node, "Memo", this.Memo);
            Loader.ModifyNode(_Node, "Country", this.Country);
            Loader.ModifyNode(_Node, "City", this.City);
            Loader.ModifyNode(_Node, "Place", this.Place);
            Loader.ModifyNode(_Node, "Notes", this.Notes);
            Loader.ModifyNode(_Node, "Id", this.Id.ToString());
            UpdateEvents();
        }

        private void UpdateEvents()
        {
            if (_Node["Events"] == null)
                Loader.ModifyNode(_Node, "Events", null);
            _Node["Events"].RemoveAll();
            foreach (int i in Events)
            {
                XmlNode ev = Loader._Doc.CreateElement("IdEvent");
                ev.InnerText = i.ToString();
                _Node["Events"].AppendChild(ev);
            }
        }

        /// <summary>
        /// Обновить событие
        /// </summary>
        public void AlterEvent(int idEvent, bool doBind)
        {
            if (doBind)
                Events.Add(idEvent);
            else
                Events.Remove(idEvent);
            UpdateEvents();
        }

        /// <summary>
        /// Удалить
        /// </summary>
        public static void Delete(int Id)
        {
            Area area = GetAreas().First(x => x.Id == Id);
            area._Node.ParentNode.RemoveChild(area._Node);
        }

        /// <summary>
        /// Создать
        /// </summary>
        public static Area Create(string name)
        {
            int id = 1;
            if (GetAreas().Count() != 0)
                id = GetAreas().Max(x => x.Id) + 1;
            Area a = new Area(id);
            a.Name = name;            
            a.Update();
            return a;            
        }

        #endregion

    }
}
