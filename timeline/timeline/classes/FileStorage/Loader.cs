﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Reflection;

namespace Timeline
{
    static class Loader
    {

        internal static XmlDocument _Doc { get; set; }

        internal static void Load()
        {
            _Doc = new XmlDocument();
            _Doc.Load(GetFileName());
        }

        private static string GetFileName()
        {
            return Path.Combine(DataRoot(), "data.xml");
        }

        public static string DataRoot()
        {
            return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "timelines");
        }

        internal static void WriteFile()
        {
            using (XmlWriter xtw = XmlTextWriter.Create(GetFileName(), new XmlWriterSettings { OmitXmlDeclaration = false, Indent = true, Encoding = Encoding.UTF8 }))
            {
                _Doc.WriteContentTo(xtw);
            }
        }

        internal static void ModifyNode(XmlNode node, string name, string value = null, string xml = null)
        {
            if (node[name] == null)
            {
                XmlNode anode = _Doc.CreateElement(name);
                node.AppendChild(anode);
            }
            if (value != null)
                node[name].InnerText = value;
            if (xml != null)
                node[name].InnerText = xml;
        }

    }
}
