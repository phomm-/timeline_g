﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Xml;

namespace Timeline
{
    class EventLink
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// XML-узел
        /// </summary>
        private XmlNode _Node { get; set; }

        private TimeLine Timeline { get; set; }
        
        public int IdEventFrom { get; set; }

        public int IdEventTo { get; set; }
        public int color { get; set; }
        public int line_width { get; set; }

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        public EventLink(int Id, int IdTimeline)
        {
            this.Id = Id;
            Timeline = TimeLine.GetTimeLines().First(x => x.Id == IdTimeline);
            if (Timeline._Node["EventLinks"] == null)
                Timeline._Node.AppendChild(Timeline._Doc.CreateElement("EventLinks"));
            this._Node = Timeline._Node["EventLinks"].AppendChild(Timeline._Doc.CreateElement("EventLink"));
        }

        /// <summary>
        /// Конструктор по XML-узлу
        /// </summary>
        public EventLink(XmlNode Node, bool fully = true)
        {
            this._Node = Node;
            this.Id = Convert.ToInt32(this._Node["Id"].InnerText);
            this.Loading();
        }

        #endregion

        #region Методы

        static public IEnumerable<EventLink> GetEventLinks(int idTimeline)
        {
            List<EventLink> result = new List<EventLink>();
            TimeLine tl = TimeLine.GetTimeLines().First(x => x.Id == idTimeline);
            if (tl._Node["EventLinks"] != null && tl._Node["EventLinks"].HasChildNodes)
                foreach (XmlElement el in tl._Node["EventLinks"])
                {
                    EventLink ev = new EventLink(el);
                    ev.Timeline = tl;
                    result.Add(ev);
                }
            return result;
        }

        public Pen Pen { get { return new Pen(Color.FromArgb((byte)color, (byte)(color >> 8), (byte)(color >> 16)), line_width); } }

        /// <summary>
        /// Загрузить
        /// </summary>
        private void Loading()
        {

            this.IdEventFrom = Convert.ToInt32(this._Node["IdEventFrom"].InnerText);
            this.IdEventTo = Convert.ToInt32(this._Node["IdEventTo"].InnerText);
            this.color = Convert.ToInt32(this._Node["color"].InnerText);
            this.line_width = Convert.ToInt32(this._Node["line_width"].InnerText);
        }

        public void Modify(int idEventFrom, int idEventTo, int lineWidth, int color)
        {
            this.IdEventFrom = idEventFrom;
            this.IdEventTo = idEventTo;
            this.line_width = lineWidth;
            this.color = color;
        }


        /// <summary>
        /// Обновить
        /// </summary>
        public void Update()
        {   
            Timeline.ModifyNode(_Node, "IdEventFrom", this.IdEventFrom.ToString());
            Timeline.ModifyNode(_Node, "IdEventTo", this.IdEventTo.ToString());
            Timeline.ModifyNode(_Node, "line_width", this.line_width.ToString());
            Timeline.ModifyNode(_Node, "color", this.color.ToString());
            Timeline.ModifyNode(_Node, "Id", this.Id.ToString());
        }

        /// <summary>
        /// Удалить
        /// </summary>
        public static void Delete(int Id)
        {
            EventLink el = GetEventLinks(TimeLine.Line.Id).First(x => x.Id == Id);
            el._Node.ParentNode.RemoveChild(el._Node);
        }

        /// <summary>
        /// Создать
        /// </summary>
        public static EventLink Create(Event ev1, Event ev2)
        {
            int id = 1;
            if (GetEventLinks(ev1.IdTimeline).Count() != 0)
                id = GetEventLinks(ev1.IdTimeline).Max(x => x.Id) + 1;
            EventLink ev = new EventLink(id, ev1.IdTimeline);
            ev.IdEventFrom = ev1.Id;
            ev.IdEventTo = ev2.Id;
            ev.Update();
            return ev;
        }

        #endregion
    }
}
