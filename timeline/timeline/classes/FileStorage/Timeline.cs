﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Xml;

namespace Timeline
{
    class TimeLine
    {

        static private TimeLine line = null;
        static public Action<bool> TimeLineChanged;
        static public TimeLine Line
        {
            get { return line; }
            set
            {
                bool doshow = false;
                if (line == null)
                    doshow = true;
                else
                    line.Clear();
                line = value;
                if (line == null)
                    doshow = false;
                else
                {
                    line.Loading(true);
                    doshow = true;
                }
                if (TimeLineChanged != null)
                    TimeLineChanged(doshow);
            }
        }

        #region Свойства

        public static BlockObject TimelineBlock { get; private set; }
        static public void SetTimelineBlock(Point pos, Size size, Pen pen)
        {
            if (TimelineBlock != null)
                return;
            TimelineBlock = new BlockObject(BlockType.Timeline, pos, size, pen);
            blocks.Add(TimelineBlock);
        }

        /// <summary>
        /// список блоков на сцене
        /// </summary>
        static private BlockList _blocks = new BlockList();

        static public BlockList blocks { get { return _blocks; } }

        private Dictionary<BlockObject, Event> Events = new Dictionary<BlockObject, Event>();
        private Dictionary<BlockObject, Storyline> Storylines = new Dictionary<BlockObject, Storyline>();
        private Dictionary<BlocksLink, EventLink> EventLinks = new Dictionary<BlocksLink, EventLink>();

        internal void Clear(bool full = true)
        {
            Events.Clear();
            Storylines.Clear();
            EventLinks.Clear();
            if (full)
            {
                blocks.Clear();
                TimelineBlock = null;
            }
            else
                blocks.ClearAllBlocks();
        }

        private static List<TimeLine> timelines = null;
        
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// XML-узел
        /// </summary>
        public XmlNode _Node { get; private set; }

        internal XmlDocument _Doc { get; set; }

        /// <summary>
        /// Навание
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Старт дата
        /// </summary>
        public DateTime? DateStart { get; set; }

        /// <summary>
        /// Конец дата
        /// </summary>
        public DateTime? DateEnd { get; set; }

        /// <summary>
        /// Информация
        /// </summary>
        public string Memo { get; set; }

        public int Len { get; set; }
        public int Y { get; set; }
        public int Color { get; set; }
        public int? IntervalValue { get; set; }
        public int? IdTimepart { get; set; }

        private string fileName { get; set; }


        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        public TimeLine(int Id)
        {
            this.Id = Id;
            this._Doc = new XmlDocument();
            this._Node = _Doc.AppendChild(_Doc.CreateElement("Timeline"));
        }

        /// <summary>
        /// Конструктор по XML-узлу
        /// </summary>
        public TimeLine(XmlNode Node, bool fully = true)
        {
            this._Node = Node;
            this.Id = Convert.ToInt32(this._Node["Id"].InnerText);
            this.Loading(fully);
        }

        #endregion

        #region Методы

        static public IEnumerable<TimeLine> GetTimeLines()
        {
            if (timelines != null)
                return timelines;
            List<TimeLine> result = new List<TimeLine>();
            string AppPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string[] files = Directory.GetFiles(Path.Combine(AppPath, "timelines"), "*.xml");
            foreach (string file in files)
            {
                XmlDocument xml = new XmlDocument();
                xml.Load(file);                
                if (xml.DocumentElement.Name == "Timeline")
                {
                    TimeLine tl = new TimeLine(xml.DocumentElement, false);
                    tl.fileName = file;
                    tl._Doc = xml;
                    result.Add(tl);                    
                }
            }
            timelines = result;
            return result;
        }

        public void RegBlock(BlockObject block = null, BlocksLink link = null)
        {
            if (block != null)
                switch (block.blockType)
                {
                    case BlockType.Hex:
                    case BlockType.Rect:
                    case BlockType.Elps:
                    case BlockType.Romb:
                        Events.Add(block, Event.Create(block.DataValue, Line.Id));
                        break;
                    case BlockType.Line:
                        Storylines.Add(block, Storyline.Create(block.DataValue, Line.Id));
                        break;
                    default:
                        break;
                }
            else if (link != null)
                EventLinks.Add(link, EventLink.Create(Events[link.blockFirst], Events[link.blockEnd]));
        }

        public static int? CheckedInt(XmlElement Node)
        {
            return Node == null || Node.InnerText == "" ? null : (int?)Convert.ToInt32(Node.InnerText);
        }

        public static DateTime? CheckedDT(XmlElement Node)
        {
            return Node == null || Node.InnerText == "" ? null : (DateTime?)Convert.ToDateTime(Node.InnerText);
        }

        /// <summary>
        /// Загрузить
        /// </summary>
        private void Loading(bool fully = true)
        {
            this.Name = Convert.ToString(this._Node["Name"].InnerText);
            this.Memo = Convert.ToString(this._Node["Memo"].InnerText);
            this.Len = Convert.ToInt32(this._Node["Len"].InnerText);
            this.Y = Convert.ToInt32(this._Node["Y"].InnerText);
            this.Color = Convert.ToInt32(this._Node["Color"].InnerText);

            this.IdTimepart = CheckedInt(this._Node["IdTimepart"]);
            this.IntervalValue = CheckedInt(this._Node["IntervalValue"]);
            this.DateStart = CheckedDT(this._Node["DateStart"]);
            this.DateEnd = CheckedDT(this._Node["DateEnd"]);

            if (!fully)
                return;
            
            foreach (Storyline line in Storyline.GetStorylines(this.Id))
            {
                BlockObject block = new BlockObject(BlockType.Line, line.StartPoint, line.Size, line.Pen);
                block.SetDataValue(line.Name);
                blocks.Add(block, false);
                Storylines.Add(block, line);
            }
            foreach (Event ev in Event.GetEvents(this.Id))
            {
                BlockObject block = new BlockObject(ev.Blocktype, ev.StartPoint, ev.Size, ev.Pen);
                block.SetDataValue(ev.Name);
                blocks.Add(block, false);
                KeyValuePair<BlockObject, Storyline> pair = Storylines.FirstOrDefault(x => x.Value.Id == ev.IdStoryline);
                if (pair.Key != null)
                    pair.Key.AddChild(block);
                Events.Add(block, ev);
            }
            foreach (EventLink link in EventLink.GetEventLinks(this.Id))
            {
                BlocksLink blink = new BlocksLink(Events.FirstOrDefault(x => x.Value.Id == link.IdEventFrom).Key,
                    Events.FirstOrDefault(x => x.Value.Id == link.IdEventTo).Key, link.Pen);
                EventLinks.Add(blink, link);
                blink.Register(false);
                blink.FindOptimalPoints();
            }
             
        }

        private int ColorToInt(Color color)
        {
            return color.R + color.G * 256 + color.B * 65536;
        }

        public void Modify(string name, DateTime? dateStart, DateTime? dateEnd, string memo, int len, int? intervalValue, int? idTimepart)
        {
            Name = name;
            Memo = memo;
            DateStart = dateStart;
            DateEnd = dateEnd;
            Len = len;
            IntervalValue = intervalValue;
            IdTimepart = idTimepart;
        }

        public void Save()
        {

            Update();
            foreach (KeyValuePair<BlockObject, Event> pair in Events)
            {
                if (!blocks.Contains(pair.Key))
                    Event.Delete(pair.Value.Id);
                else
                {
                    pair.Value.Modify(pair.Key.X, pair.Key.Y, pair.Key.Width, pair.Key.Height, ColorToInt(pair.Key.blockPen.Color),
                        (int)pair.Key.blockPen.Width, pair.Key.IsAtLine ? Storylines[pair.Key.parent].Id : (int?)null);
                    pair.Value.Update();
                }
            }
            foreach (KeyValuePair<BlocksLink, EventLink> pair in EventLinks)
            {
                if (!blocks.Contains(pair.Key.blockFirst) || !blocks.Contains(pair.Key.blockEnd) || !pair.Key.blockFirst.Links.Contains(pair.Key))
                    EventLink.Delete(pair.Value.Id);
                else
                {
                    pair.Value.Modify(Events[pair.Key.First].Id, Events[pair.Key.Second].Id, ColorToInt(pair.Key.linkPen.Color), (int)pair.Key.linkPen.Width);
                    pair.Value.Update();
                }

            }
            foreach (KeyValuePair<BlockObject, Storyline> pair in Storylines)
            {
                if (!blocks.Contains(pair.Key))
                    Storyline.Delete(pair.Value.Id);
                else
                {
                    pair.Value.Modify(pair.Key.Y, pair.Key.Height, ColorToInt(pair.Key.blockPen.Color), (int)pair.Key.blockPen.Width, pair.Key.Width);
                    pair.Value.Update();
                }
            }
            WriteFile();
            Loader.WriteFile();
        }

        private static string GetFileName(TimeLine t)
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "timelines");
            string xmlfilename = string.IsNullOrEmpty(t.fileName) ? t.Name + ".xml" : t.fileName;
            return Path.Combine(path, xmlfilename);
        }

        private void WriteFile()
        {
            using (XmlWriter xtw = XmlTextWriter.Create(GetFileName(this), new XmlWriterSettings { OmitXmlDeclaration = false, Indent = true, Encoding = Encoding.UTF8 }))
            {
                _Doc.WriteContentTo(xtw);
            }      
        }

        internal void ModifyNode(XmlNode node, string name, string value)
        {
            if (node[name] == null)
            {
                XmlNode anode =  _Doc.CreateElement(name);
                node.AppendChild(anode);
            }
            node[name].InnerText = value;
        }

        /// <summary>
        /// Обновить
        /// </summary>
        private void Update()
        {
            ModifyNode(_Node, "Name", this.Name);
            ModifyNode(_Node, "Memo", this.Memo);
            ModifyNode(_Node, "IdTimepart", this.IdTimepart.ToString());
            ModifyNode(_Node, "IntervalValue", this.IntervalValue.ToString());
            ModifyNode(_Node, "Len", this.Len.ToString());
            ModifyNode(_Node, "Y", this.Y.ToString());
            ModifyNode(_Node, "Color", this.Color.ToString());
            ModifyNode(_Node, "DateStart", this.DateStart.ToString());
            ModifyNode(_Node, "DateEnd", this.DateEnd.ToString());
            ModifyNode(_Node, "Id", this.Id.ToString());
            WriteFile();
        }

        /// <summary>
        /// Удалить
        /// </summary>
        public static void Delete(int Id)
        {
            TimeLine t = GetTimeLines().First(x => x.Id == Id);
            if (t != null)
            {
                File.Delete(GetFileName(t));
                timelines.Remove(t);
            }
        }

        /// <summary>
        /// Создать
        /// </summary>
        public static TimeLine Create(string name)
        {
            int id;
            TimeLine t = null;
            IEnumerable<TimeLine> lines = GetTimeLines();
            if (lines.Count(x => x.Name == name) != 0)
            {
                Prompt.Message("Нельзя создавать хронику с именем, дублирующим существующую хронику", "Ошибка");
            }
            else 
            {
                id = lines.Max(x => x.Id) + 1;
                t = new TimeLine(id);
                t.Name = name;
                t.Len = Consts.MinTimelineWidth;
                t.Update();
                timelines.Add(t);
            }
            return t;
        }

        public Event EventFromBlock(BlockObject block)
        {
            return Events[block];
        }

        public Storyline StoryLineFromBlock(BlockObject block)
        {
            return Storylines[block];
        }

        #endregion
    }
}
