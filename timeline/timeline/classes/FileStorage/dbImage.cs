﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace Timeline
{

    /// <summary>
    /// картинки
    /// </summary>
    public class dbImage
    {


        #region Свойства

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// XML-узел
        /// </summary>
        private XmlNode _Node { get; set; }

        
        /// <summary>
        /// имя
        /// </summary>
        public string Name { get; set; }

        public string FileName { get; set; }


        /// <summary>
        /// Информация
        /// </summary>
        public string Memo { get; set; }

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int? IdEvent { get; private set; }

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int? IdPerson { get; private set; }

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int? IdArea { get; private set; }

        /// <summary>
        /// Байты
        /// </summary>
        public byte[] Bytes { get; set; }

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        public dbImage(int Id)
        {
            this.Id = Id;
            if (Loader._Doc.DocumentElement["Images"] == null)
                Loader._Doc.DocumentElement.AppendChild(Loader._Doc.CreateElement("Images"));
            this._Node = Loader._Doc.DocumentElement["Images"].AppendChild(Loader._Doc.CreateElement("Image"));
        }

        /// <summary>
        /// Конструктор по XML-узлу
        /// </summary>
        public dbImage(XmlNode Node)
        {
            this._Node = Node;
            this.Id = Convert.ToInt32(this._Node["Id"].InnerText);
            this.Loading();
        }

        #endregion

        #region Методы

        static public IEnumerable<dbImage> GetImages()
        {
            List<dbImage> result = new List<dbImage>();
            if (Loader._Doc.DocumentElement["Images"] != null)
                foreach (XmlNode node in Loader._Doc.DocumentElement["Images"])
                    result.Add(new dbImage(node));
            return result;
        }

        /// <summary>
        /// Загрузить
        /// </summary>
        private void Loading()
        {
            this.Name = Convert.ToString(this._Node["Name"].InnerText);
            this.Memo = Convert.ToString(this._Node["Memo"].InnerText);
            this.FileName = this._Node["FileName"].InnerText;
            this.Bytes = File.ReadAllBytes(Path.Combine(Loader.DataRoot(), FileName));
            this.IdEvent = TimeLine.CheckedInt(this._Node["IdEvent"]);
            this.IdPerson = TimeLine.CheckedInt(this._Node["IdPerson"]);
            this.IdArea = TimeLine.CheckedInt(this._Node["IdArea"]);
        }

        public bool IsImageBound(int entity, int idEntity)
        {
            switch (entity)
            {
                case 0: return IdEvent == idEntity;
                case 1: return IdPerson == idEntity;
                case 2: return IdArea == idEntity;
                default: return false;
            }
        }

        /// <summary>
        /// Обновить
        /// </summary>
        public void Update()
        {
            Loader.ModifyNode(_Node, "Name", this.Name);
            Loader.ModifyNode(_Node, "Memo", this.Memo);
            Loader.ModifyNode(_Node, "FileName", this.FileName);
            Loader.ModifyNode(_Node, "IdEvent", this.IdEvent.ToString());
            Loader.ModifyNode(_Node, "IdPerson", this.IdPerson.ToString());
            Loader.ModifyNode(_Node, "IdArea", this.IdArea.ToString());
            Loader.ModifyNode(_Node, "Id", this.Id.ToString());
        }

        /// <summary>
        /// Обновить привязку картинки к сущности
        /// </summary>
        public void AlterBind(int entity, int? idEntity)
        {
            switch (entity)
            {
                case 0: IdEvent = idEntity;
                    break;
                case 1: IdPerson = idEntity;
                    break;
                case 2: IdArea = idEntity;
                    break;
            }
            Update();
        }

        /// <summary>
        /// Удалить
        /// </summary>
        public static void Delete(int Id)
        {
            dbImage image = GetImages().First(x => x.Id == Id);
            image._Node.ParentNode.RemoveChild(image._Node);
            File.Delete(image.FileName);
        }

        /// <summary>
        /// Создать
        /// </summary>
        public static dbImage Create(string filename)
        {
            int id = 1;
            if (GetImages().Count() != 0)
                id = GetImages().Max(x => x.Id) + 1;
            dbImage p = new dbImage(id);
            string dest = Path.Combine(Loader.DataRoot(), Path.GetFileName(filename));
            int i = 0;
            while (File.Exists(dest) && i++ < 100)
                dest = Path.Combine(Loader.DataRoot(), Path.GetFileNameWithoutExtension(filename) + i.ToString() + Path.GetExtension(dest));
            File.Copy(filename, dest);
            p.FileName = Path.GetFileName(dest);
            p.Update();
            return p; 
        }

        #endregion

    }
}
