﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace Timeline
{
    /// <summary>
    /// Объектом класса BlocksLink является связь между блоками
    /// </summary>
    public class BlocksLink
    {
        #region ' переменные '
        /// <summary>
        /// два блока, которые связаны данной связью
        /// </summary>
        public BlockObject blockFirst { get; private set; }
        public BlockObject blockEnd { get; private set; }

        /// <summary>
        /// перо для рисования связи
        /// </summary>
        public Pen linkPen { get; private set; }

        /// <summary>
        /// точки блоков, к которым привязана связь (расчитываются при создании свзи и перемещении блоков в методе GetPoints
        /// </summary>
        public Point anchorPointFisrt, anchorPointEnd;
        #endregion

        #region ' конструктор '
        public BlocksLink(BlockObject first, Pen lnpen) : this(first, null, lnpen) { }
        public BlocksLink(BlockObject first, BlockObject second, Pen lnpen)
        {
            blockFirst = first;
            blockEnd = second;
            linkPen = lnpen;
            linkPen.Width = 2;
        }
        #endregion

        #region ' публичные методы для предоставления данных о связи '
        public BlockObject First
        {
            get
            {
                return blockFirst;
            }
            set
            {
                blockFirst = value;
            }
        }
        public BlockObject Second
        {
            get
            {
                return blockEnd;
            }
            set
            {
                blockEnd = value;
            }
        }
        #endregion

        /// <summary>
        /// регистрируем связь в блоках, если оба заданы
        /// </summary> 
        public void Register(bool isnew)
        {
            if (allBlocksOK())
            {
                blockFirst.RegisterLink(this);
                blockEnd.RegisterLink(this);
                if (isnew)
                    TimeLine.Line.RegBlock(null, this);
            }
        }

        /// <summary>
        /// уничтожаем ссылку
        /// </summary>
        public void Remove(BlockObject sender)
        {
            if (blockFirst != sender)
            {
                blockFirst.RemoveLink(this);
            }
            if (blockEnd != sender)
            {
                blockEnd.RemoveLink(this);
            }
        }

        /// <summary>
        /// рисуем связь
        /// </summary>
        public void DrawIt(Graphics g, PictureBox picBox)
        {
            if (allBlocksOK())
            {
                if ((anchorPointFisrt == null) || (anchorPointEnd == null)) FindOptimalPoints();
                g = Graphics.FromImage(picBox.Image);
                g.DrawLine(linkPen, anchorPointFisrt, anchorPointEnd);
                //!!! СТРЕЛОЧКА
                // вот тут, по хорошему, рисуются еще две линии, для того, чтобы получилась стрелочка, но,
                // с тригонометрией так не охота связываться, что жуть - ведь надо расчитать угол наклона стрелочки
                // cos A = (anchorPointEnd.X - anchorPointFisrt.X) / calcDistance(anchorPointFisrt, anchorPointEnd) в случае,
                // если стрелка смотрит снизу-вверх и слева-направо, а потом еще расчитать две точки, которые образовали линии,
                // отклоненные от линии связи на 30 градусов (cos 30 градусов = Math.Sqrt(3) / 2).. Мозг уже не варит..
                g.FillEllipse(linkPen.Brush, anchorPointEnd.X - 4, anchorPointEnd.Y - 4, 8, 8);
                g.Dispose();
            }
        }

        /// <summary>
        /// вычисляем и записываем оптимальные точки между двумя блоками, если они заданы
        /// </summary> 
        public void FindOptimalPoints()
        {
            if (allBlocksOK())
            {
                double disctanceA = -1;
                Point pS = new Point();
                Point pE = new Point();

                checkPointsOptimal("top", ref pS, ref pE, ref disctanceA);
                checkPointsOptimal("right", ref pS, ref pE, ref disctanceA);
                checkPointsOptimal("bottom", ref pS, ref pE, ref disctanceA);
                checkPointsOptimal("left", ref pS, ref pE, ref disctanceA);
                // сохраним, в том числе, чтобы не пересчитывать каждый раз при перерисовке
                anchorPointFisrt = new Point(pS.X, pS.Y);
                anchorPointEnd = new Point(pE.X, pE.Y);
            }
        }

        /// <summary>
        /// находим оптимальную точку первого блока, относительно переданной точке и возвращаем её в ответе
        /// </summary>
        public Point GetStartPoint(Point cpt)
        {
            Point answer = new Point();
            double distanceA = -1;

            checkLinkPoint("top", cpt, blockFirst, ref answer, ref distanceA);
            checkLinkPoint("right", cpt, blockFirst, ref answer, ref distanceA);
            checkLinkPoint("bottom", cpt, blockFirst, ref answer, ref distanceA);
            checkLinkPoint("left", cpt, blockFirst, ref answer, ref distanceA);

            return answer;
        }

        #region ' частные вспомогательные методы для определения точек связи '
        /// <summary>
        /// метод ищет оптимальную точку первого блока для заданной точки последнего и сравнивает дистанцию между ними с переданной
        /// если дистанция меньше переданной distanceA или distanceA еще не задана (-1), записываем найденные точки и новую дистанцию
        /// </summary>
        private void checkPointsOptimal(string LinkPointType, ref Point pS, ref Point pE, ref double disctanceA)
        {
            double disctanceB;
            Point newS, newE;
            newE = getLinkPoint(LinkPointType, blockEnd);
            newS = GetStartPoint(newE);
            disctanceB = calcDistance(newS, newE);

            if ((disctanceB < disctanceA) || (disctanceA == -1))
            {
                disctanceA = disctanceB;
                pS = newS;
                pE = newE;
            }
        }

        /// <summary>
        /// метод ищет оптимальную точку блока для переданной точки и сравнивает дистанцию между ними с переданной дистанцией
        /// если дистанция меньше переданной distanceA или distanceA еще не задана (-1), записываем найденную точку и новую дистанцию
        /// </summary>
        private void checkLinkPoint(string LinkPointType, Point cpt, BlockObject bl, ref Point answer, ref double distanceA)
        {
            double distanceB;
            Point myP = getLinkPoint(LinkPointType, bl);
            distanceB = calcDistance(myP, cpt);

            if ((distanceB < distanceA) || (distanceA == -1))
            {
                distanceA = distanceB;
                answer.X = myP.X;
                answer.Y = myP.Y;
            }
        }

        /// <summary>
        /// возвращает точку связи блока в зависимости от переданного LinkPointType {"top", "right", "bottom", "left"}
        /// </summary>
        private Point getLinkPoint(string LinkPointType, BlockObject bl)
        {
            Point answer = new Point();
            switch (LinkPointType)
            {
                case "top":
                    answer.X = bl.X + (bl.Width / 2);
                    answer.Y = bl.Y;
                    break;
                case "right":
                    answer.X = bl.X + bl.Width;
                    answer.Y = bl.Y + (bl.Height / 2);
                    break;
                case "bottom":
                    answer.X = bl.X + (bl.Width / 2);
                    answer.Y = bl.Y + bl.Height;
                    break;
                case "left":
                    answer.X = bl.X;
                    answer.Y = bl.Y + (bl.Height / 2);
                    break;
                default:
                    break;
            }
            return answer;
        }

        /// <summary>
        /// расчет дистанции (расстояния между двумя точками)
        /// </summary>
        private double calcDistance(Point p1, Point p2)
        {
            // Thanks for Pythagoras :)
            return Math.Sqrt((Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2)));
        }
        #endregion


        /// <summary>
        /// проверяем, заданы ли блоки
        /// </summary>
        private bool allBlocksOK()
        {
            return (blockFirst != null) && (blockEnd != null);
        }
    }
}
