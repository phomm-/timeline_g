﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Timeline
{
    public partial class ImagesForm : Form
    {
        public int IdEntity { get; private set; }
        private int Entity;
        public ImagesForm()
        {
            InitializeComponent();
            lstImages.CheckBoxes = true;
            lstImages.View = View.Details;
            lstImages.Columns.Add(Consts.FieldName, 150);
            lstImages.Columns.Add(Consts.FieldInfo, 400);
        }

        public void Open(int entity, int idEntity, string caption)
        {
            Entity = entity;
            IdEntity = idEntity;
            Text = "Управление картинками. Галочка - привязка к " + caption;
            Requery();
            ShowDialog();
        }

        public void Requery()
        {
            lstImages.Items.Clear();
            foreach (dbImage data in dbImage.GetImages())
            {
                ListViewItem item = new ListViewItem();
                item.Checked = data.IsImageBound(Entity, IdEntity);
                item.Text = Convert.ToString(data.Name);
                item.SubItems.Add(Convert.ToString(data.Memo));
                item.Tag = data;
                lstImages.Items.Add(item);
            }
            if (lstImages.Items.Count > 0)
                lstImages.Items[0].Selected = true;
        }


        private void btnCreateImage_Click(object sender, EventArgs e)
        {
            AddImage form = new AddImage();
            form.Open(null);
            Requery();
        }

        private void btnDeleteImage_Click(object sender, EventArgs e)
        {
            if (lstImages.Items.Count > 0 && lstImages.SelectedItems.Count == 1)
            {
                dbImage data = (dbImage)lstImages.SelectedItems[0].Tag;
                if (Prompt.Confirm("Удалить картинку " + data.Name + " ?", "Внимание !"))
                {
                    dbImage.Delete(data.Id);
                    Requery();
                }
            }
        }

        private void btnEditImage_Click(object sender, EventArgs e)
        {
            if (lstImages.Items.Count > 0 && lstImages.SelectedItems.Count == 1)
            {
                dbImage data = (dbImage)lstImages.SelectedItems[0].Tag;
                AddImage form = new AddImage();
                form.Open(data);
                Requery();
            }
        }

        private void lstImages_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (lstImages.Items.Count > 0 && lstImages.SelectedItems.Count == 1)
            {
                dbImage data = (dbImage)lstImages.SelectedItems[0].Tag;
                picImage.Image = ImageWorks.FromByteArray(data.Bytes);
            }
        }

        private void lstImages_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            dbImage data = (dbImage)e.Item.Tag;
            data.AlterBind(Entity, e.Item.Checked ? IdEntity : (int?)null);
        }

    }
}
