﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Timeline
{
    public partial class AreaItem : Form
    {

        //Персон
        private Area _Area;

        /// <summary>
        /// Конструктор
        /// </summary>
        public AreaItem(Area Area)
        {
            InitializeComponent();
            this._Area = Area;
        }

        //Загрузка
        private void frmAreaItem_Load(object sender, EventArgs e)
        {
            if (this._Area != null)
            {
                txtName.Text = this._Area.Name;
                txtMemo.Text = this._Area.Memo;
                txtCountry.Text = this._Area.Country;
                txtCity.Text = this._Area.City;
                txtPlace.Text = this._Area.Place;
                txtNotes.Text = this._Area.Notes;
            }
        }

        //Сохранить
        private void butOK_Click(object sender, EventArgs e)
        {
            //Создаём
            if (this._Area == null)
                this._Area = Area.Create(txtName.Text);

            //Сохраняем
            this._Area.Name = txtName.Text;
            this._Area.Memo = txtMemo.Text;
            this._Area.Country = txtCountry.Text;
            this._Area.City = txtCity.Text;
            this._Area.Place = txtPlace.Text;
            this._Area.Notes = txtNotes.Text;

            this._Area.Update();

            //Выход
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        //Отмена
        private void butCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
