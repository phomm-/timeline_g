﻿namespace Timeline
{
    partial class PersonsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstPersons = new System.Windows.Forms.ListView();
            this.btnCreateNew = new System.Windows.Forms.Button();
            this.btnDeleteTimeline = new System.Windows.Forms.Button();
            this.btnPersonImages = new System.Windows.Forms.Button();
            this.btnEditPerson = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lstPersons
            // 
            this.lstPersons.Dock = System.Windows.Forms.DockStyle.Top;
            this.lstPersons.Location = new System.Drawing.Point(0, 0);
            this.lstPersons.Name = "lstPersons";
            this.lstPersons.Size = new System.Drawing.Size(535, 341);
            this.lstPersons.TabIndex = 0;
            this.lstPersons.UseCompatibleStateImageBehavior = false;
            this.lstPersons.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lstPersons_ItemChecked);
            this.lstPersons.SelectedIndexChanged += new System.EventHandler(this.lstPersons_SelectedIndexChanged);
            // 
            // btnCreateNew
            // 
            this.btnCreateNew.Location = new System.Drawing.Point(95, 373);
            this.btnCreateNew.Name = "btnCreateNew";
            this.btnCreateNew.Size = new System.Drawing.Size(75, 23);
            this.btnCreateNew.TabIndex = 1;
            this.btnCreateNew.Text = "Создать";
            this.btnCreateNew.UseVisualStyleBackColor = true;
            this.btnCreateNew.Click += new System.EventHandler(this.btnCreatePerson_Click);
            // 
            // btnDeleteTimeline
            // 
            this.btnDeleteTimeline.Location = new System.Drawing.Point(405, 373);
            this.btnDeleteTimeline.Name = "btnDeleteTimeline";
            this.btnDeleteTimeline.Size = new System.Drawing.Size(61, 23);
            this.btnDeleteTimeline.TabIndex = 2;
            this.btnDeleteTimeline.Text = "Удалить";
            this.btnDeleteTimeline.UseVisualStyleBackColor = true;
            this.btnDeleteTimeline.Click += new System.EventHandler(this.btnDeletePerson_Click);
            // 
            // btnPersonImages
            // 
            this.btnPersonImages.Location = new System.Drawing.Point(245, 373);
            this.btnPersonImages.Name = "btnPersonImages";
            this.btnPersonImages.Size = new System.Drawing.Size(129, 23);
            this.btnPersonImages.TabIndex = 4;
            this.btnPersonImages.Text = "Картинки персонажа";
            this.btnPersonImages.UseVisualStyleBackColor = true;
            this.btnPersonImages.Click += new System.EventHandler(this.btnPersonImages_Click);
            // 
            // btnEditPerson
            // 
            this.btnEditPerson.Location = new System.Drawing.Point(178, 373);
            this.btnEditPerson.Name = "btnEditPerson";
            this.btnEditPerson.Size = new System.Drawing.Size(61, 23);
            this.btnEditPerson.TabIndex = 5;
            this.btnEditPerson.Text = "Правка";
            this.btnEditPerson.UseVisualStyleBackColor = true;
            this.btnEditPerson.Click += new System.EventHandler(this.btnEditPerson_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 350);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Роль персонажа";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(120, 347);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(415, 20);
            this.txtName.TabIndex = 8;
            this.txtName.Leave += new System.EventHandler(this.txtName_Leave);
            // 
            // PersonsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 398);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.btnEditPerson);
            this.Controls.Add(this.btnPersonImages);
            this.Controls.Add(this.btnDeleteTimeline);
            this.Controls.Add(this.btnCreateNew);
            this.Controls.Add(this.lstPersons);
            this.Name = "PersonsForm";
            this.Text = "PersonsForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lstPersons;
        private System.Windows.Forms.Button btnCreateNew;
        private System.Windows.Forms.Button btnDeleteTimeline;
        private System.Windows.Forms.Button btnPersonImages;
        private System.Windows.Forms.Button btnEditPerson;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
    }
}