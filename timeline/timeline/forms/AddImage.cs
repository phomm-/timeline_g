﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Timeline
{
    public partial class AddImage : Form
    {


        //Картинка
        private dbImage Image;
        private string filename;

        public AddImage()
        {
            InitializeComponent();
        }

        public void Open(dbImage image)
        {
            Image = image;
            ShowDialog();
        }

        //Сохранить
        private void butOK_Click(object sender, EventArgs e)
        {
            //Создаём
            if (this.Image == null)
            {
                this.Image = dbImage.Create(filename);
            }

            //Сохраняем
            this.Image.Name = txtName.Text;
            this.Image.Memo = txtMemo.Text;
            this.Image.Bytes = ImageWorks.ToByteArray(picImage.Image);
            this.Image.Update();


            //Выход
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        //Отмена
        private void butCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Отрыть картинку
        private void butSelect_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog d = new OpenFileDialog())
            {
                d.Filter = "JPG файл (*.jpg)|*.jpg";
                if (d.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    picImage.Image = ImageWorks.FromByteArray(System.IO.File.ReadAllBytes(d.FileName));
                    filename = d.FileName;
                    butOK.Enabled = true;
                }
            }
        }

        //Загрузка
        private void frmPersonsImage_Load(object sender, EventArgs e)
        {
            if (this.Image != null)
            {
                txtName.Text = this.Image.Name;
                txtMemo.Text = this.Image.Memo;
                picImage.Image = ImageWorks.FromByteArray(this.Image.Bytes);
                butOK.Enabled = true;
            }
        }

    }
}
