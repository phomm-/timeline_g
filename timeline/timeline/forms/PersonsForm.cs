﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Timeline
{
    public partial class PersonsForm : Form
    {
        public Event BindEvent { get; private set; }
        public PersonsForm()
        {
            InitializeComponent();
            lstPersons.CheckBoxes = true;
            lstPersons.View = View.Details;
            lstPersons.Columns.Add(Consts.PersonName, 150);
            lstPersons.Columns.Add(Consts.FieldInfo, 400);
        }

        public void Open(Event bindEvent)
        {
            BindEvent = bindEvent;
            Text = "Управление персонажами. Галочка - привязка к " + BindEvent.Name;
            Requery();
            ShowDialog();
        }

        public void Requery()
        {
            lstPersons.Items.Clear();
            foreach (Person data in Person.GetPersons())
            {
                ListViewItem item = new ListViewItem();
                item.Checked = data.IsEventPerson(BindEvent.Id);
                item.Text = Convert.ToString(data.Name);
                item.SubItems.Add(Convert.ToString(data.Memo));
                item.Tag = data;
                lstPersons.Items.Add(item);
            }
            if (lstPersons.Items.Count > 0)
                lstPersons.Items[0].Selected = true;
        }


        private void btnCreatePerson_Click(object sender, EventArgs e)
        {
            PersonsItem form = new PersonsItem(null);
            form.ShowDialog();
            Requery();
        }

        private void btnPersonImages_Click(object sender, EventArgs e)
        {
            if (lstPersons.Items.Count > 0 && lstPersons.SelectedItems.Count == 1)
            {
                Person data = (Person)lstPersons.SelectedItems[0].Tag;
                ImagesForm form = new ImagesForm();
                form.Open(1, data.Id, data.Name);
            }
        }

        private void btnDeletePerson_Click(object sender, EventArgs e)
        {
            if (lstPersons.Items.Count > 0 && lstPersons.SelectedItems.Count == 1)
            {
                Person data = (Person)lstPersons.SelectedItems[0].Tag;
                if (Prompt.Confirm("Удалить персонажа " + data.Name + " ?", "Внимание !"))
                {
                    Person.Delete(data.Id);
                    Requery();
                }
            }
        }

        private void btnEditPerson_Click(object sender, EventArgs e)
        {
            if (lstPersons.Items.Count > 0 && lstPersons.SelectedItems.Count == 1)
            {
                Person data = (Person)lstPersons.SelectedItems[0].Tag;
                PersonsItem form = new PersonsItem(data);
                form.ShowDialog();
                Requery();
            }
        }

        private void lstPersons_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            Person data = (Person)e.Item.Tag;
            data.AlterEvent(BindEvent.Id, e.Item.Checked);
        }

        private void lstPersons_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstPersons.Items.Count > 0 && lstPersons.SelectedItems.Count == 1)
            {
                Person data = (Person)lstPersons.SelectedItems[0].Tag;
                txtName.Text = "";
                txtName.Enabled = lstPersons.SelectedItems[0].Checked;
                txtName.Text = data.GetPersonRole(BindEvent.Id);
            }
        }

        private void txtName_Leave(object sender, EventArgs e)
        {
            if (lstPersons.Items.Count > 0 && lstPersons.SelectedItems.Count == 1)
            {
                Person data = (Person)lstPersons.SelectedItems[0].Tag;
                data.AlterPersonRole(BindEvent.Id, txtName.Text);
            }
        }


    }
}
