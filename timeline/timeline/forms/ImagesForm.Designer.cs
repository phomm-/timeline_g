﻿namespace Timeline
{
    partial class ImagesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstImages = new System.Windows.Forms.ListView();
            this.btnCreateNew = new System.Windows.Forms.Button();
            this.btnDeleteTimeline = new System.Windows.Forms.Button();
            this.btnEditImage = new System.Windows.Forms.Button();
            this.picImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            this.SuspendLayout();
            // 
            // lstImages
            // 
            this.lstImages.Dock = System.Windows.Forms.DockStyle.Top;
            this.lstImages.Location = new System.Drawing.Point(0, 0);
            this.lstImages.Name = "lstImages";
            this.lstImages.Size = new System.Drawing.Size(513, 295);
            this.lstImages.TabIndex = 0;
            this.lstImages.UseCompatibleStateImageBehavior = false;
            this.lstImages.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lstImages_ItemChecked);
            this.lstImages.SelectedIndexChanged += new System.EventHandler(this.lstImages_SelectedIndexChanged);
            // 
            // btnCreateNew
            // 
            this.btnCreateNew.Location = new System.Drawing.Point(129, 595);
            this.btnCreateNew.Name = "btnCreateNew";
            this.btnCreateNew.Size = new System.Drawing.Size(75, 23);
            this.btnCreateNew.TabIndex = 1;
            this.btnCreateNew.Text = "Создать";
            this.btnCreateNew.UseVisualStyleBackColor = true;
            this.btnCreateNew.Click += new System.EventHandler(this.btnCreateImage_Click);
            // 
            // btnDeleteTimeline
            // 
            this.btnDeleteTimeline.Location = new System.Drawing.Point(336, 595);
            this.btnDeleteTimeline.Name = "btnDeleteTimeline";
            this.btnDeleteTimeline.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteTimeline.TabIndex = 2;
            this.btnDeleteTimeline.Text = "Удалить";
            this.btnDeleteTimeline.UseVisualStyleBackColor = true;
            this.btnDeleteTimeline.Click += new System.EventHandler(this.btnDeleteImage_Click);
            // 
            // btnEditImage
            // 
            this.btnEditImage.Location = new System.Drawing.Point(228, 595);
            this.btnEditImage.Name = "btnEditImage";
            this.btnEditImage.Size = new System.Drawing.Size(75, 23);
            this.btnEditImage.TabIndex = 4;
            this.btnEditImage.Text = "Правка";
            this.btnEditImage.UseVisualStyleBackColor = true;
            this.btnEditImage.Click += new System.EventHandler(this.btnEditImage_Click);
            // 
            // picImage
            // 
            this.picImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picImage.Location = new System.Drawing.Point(0, 301);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(513, 288);
            this.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picImage.TabIndex = 11;
            this.picImage.TabStop = false;
            // 
            // ImagesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 628);
            this.Controls.Add(this.picImage);
            this.Controls.Add(this.btnEditImage);
            this.Controls.Add(this.btnDeleteTimeline);
            this.Controls.Add(this.btnCreateNew);
            this.Controls.Add(this.lstImages);
            this.Name = "ImagesForm";
            this.Text = "ImagesForm";
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstImages;
        private System.Windows.Forms.Button btnCreateNew;
        private System.Windows.Forms.Button btnDeleteTimeline;
        private System.Windows.Forms.Button btnEditImage;
        private System.Windows.Forms.PictureBox picImage;
    }
}