﻿namespace Timeline
{
    partial class AreasForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstAreas = new System.Windows.Forms.ListView();
            this.btnCreateNew = new System.Windows.Forms.Button();
            this.btnDeleteTimeline = new System.Windows.Forms.Button();
            this.btnAreaImages = new System.Windows.Forms.Button();
            this.btnEditArea = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstAreas
            // 
            this.lstAreas.Dock = System.Windows.Forms.DockStyle.Top;
            this.lstAreas.Location = new System.Drawing.Point(0, 0);
            this.lstAreas.Name = "lstAreas";
            this.lstAreas.Size = new System.Drawing.Size(532, 317);
            this.lstAreas.TabIndex = 0;
            this.lstAreas.UseCompatibleStateImageBehavior = false;
            this.lstAreas.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lstAreas_ItemChecked);
            // 
            // btnCreateNew
            // 
            this.btnCreateNew.Location = new System.Drawing.Point(43, 323);
            this.btnCreateNew.Name = "btnCreateNew";
            this.btnCreateNew.Size = new System.Drawing.Size(75, 23);
            this.btnCreateNew.TabIndex = 1;
            this.btnCreateNew.Text = "Создать";
            this.btnCreateNew.UseVisualStyleBackColor = true;
            this.btnCreateNew.Click += new System.EventHandler(this.btnCreateArea_Click);
            // 
            // btnDeleteTimeline
            // 
            this.btnDeleteTimeline.Location = new System.Drawing.Point(353, 323);
            this.btnDeleteTimeline.Name = "btnDeleteTimeline";
            this.btnDeleteTimeline.Size = new System.Drawing.Size(58, 23);
            this.btnDeleteTimeline.TabIndex = 2;
            this.btnDeleteTimeline.Text = "Удалить";
            this.btnDeleteTimeline.UseVisualStyleBackColor = true;
            this.btnDeleteTimeline.Click += new System.EventHandler(this.btnDeleteArea_Click);
            // 
            // btnAreaImages
            // 
            this.btnAreaImages.Location = new System.Drawing.Point(196, 323);
            this.btnAreaImages.Name = "btnAreaImages";
            this.btnAreaImages.Size = new System.Drawing.Size(118, 23);
            this.btnAreaImages.TabIndex = 4;
            this.btnAreaImages.Text = "Картинки локации";
            this.btnAreaImages.UseVisualStyleBackColor = true;
            this.btnAreaImages.Click += new System.EventHandler(this.btnAreaImages_Click);
            // 
            // btnEditArea
            // 
            this.btnEditArea.Location = new System.Drawing.Point(124, 323);
            this.btnEditArea.Name = "btnEditArea";
            this.btnEditArea.Size = new System.Drawing.Size(58, 23);
            this.btnEditArea.TabIndex = 5;
            this.btnEditArea.Text = "Правка";
            this.btnEditArea.UseVisualStyleBackColor = true;
            this.btnEditArea.Click += new System.EventHandler(this.btnEditArea_Click);
            // 
            // AreasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 350);
            this.Controls.Add(this.btnEditArea);
            this.Controls.Add(this.btnAreaImages);
            this.Controls.Add(this.btnDeleteTimeline);
            this.Controls.Add(this.btnCreateNew);
            this.Controls.Add(this.lstAreas);
            this.Name = "AreasForm";
            this.Text = "AreasForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstAreas;
        private System.Windows.Forms.Button btnCreateNew;
        private System.Windows.Forms.Button btnDeleteTimeline;
        private System.Windows.Forms.Button btnAreaImages;
        private System.Windows.Forms.Button btnEditArea;
    }
}