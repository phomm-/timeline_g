﻿namespace Timeline
{
    partial class TimelinesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstTimelines = new System.Windows.Forms.ListView();
            this.btnOpenTimeline = new System.Windows.Forms.Button();
            this.btnDeleteTimeline = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstTimelines
            // 
            this.lstTimelines.Dock = System.Windows.Forms.DockStyle.Top;
            this.lstTimelines.Location = new System.Drawing.Point(0, 0);
            this.lstTimelines.Name = "lstTimelines";
            this.lstTimelines.Size = new System.Drawing.Size(423, 295);
            this.lstTimelines.TabIndex = 0;
            this.lstTimelines.UseCompatibleStateImageBehavior = false;
            this.lstTimelines.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstTimelines_MouseDoubleClick);
            // 
            // btnOpenTimeline
            // 
            this.btnOpenTimeline.Location = new System.Drawing.Point(17, 323);
            this.btnOpenTimeline.Name = "btnOpenTimeline";
            this.btnOpenTimeline.Size = new System.Drawing.Size(75, 23);
            this.btnOpenTimeline.TabIndex = 1;
            this.btnOpenTimeline.Text = "Открыть";
            this.btnOpenTimeline.UseVisualStyleBackColor = true;
            this.btnOpenTimeline.Click += new System.EventHandler(this.btnOpenTimeline_Click);
            // 
            // btnDeleteTimeline
            // 
            this.btnDeleteTimeline.Location = new System.Drawing.Point(336, 323);
            this.btnDeleteTimeline.Name = "btnDeleteTimeline";
            this.btnDeleteTimeline.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteTimeline.TabIndex = 2;
            this.btnDeleteTimeline.Text = "Удалить";
            this.btnDeleteTimeline.UseVisualStyleBackColor = true;
            this.btnDeleteTimeline.Click += new System.EventHandler(this.btnDeleteTimeline_Click);
            // 
            // TimelinesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 358);
            this.Controls.Add(this.btnDeleteTimeline);
            this.Controls.Add(this.btnOpenTimeline);
            this.Controls.Add(this.lstTimelines);
            this.Name = "TimelinesForm";
            this.Text = "Хроники";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstTimelines;
        private System.Windows.Forms.Button btnOpenTimeline;
        private System.Windows.Forms.Button btnDeleteTimeline;
    }
}