﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Timeline
{
    public partial class TimelinesForm : Form
    {
        public TimelinesForm()
        {
            InitializeComponent();
            lstTimelines.View = View.Details;
            lstTimelines.Columns.Add("Имя", 100);
            lstTimelines.Columns.Add("Описание", 300);
            Requery();
        }

        public void Requery()
        {
            lstTimelines.Items.Clear();
            foreach (TimeLine data in TimeLine.GetTimeLines())
            {
                ListViewItem item = new ListViewItem();
                item.Text = Convert.ToString(data.Name);
                item.SubItems.Add(Convert.ToString(data.Memo));
                item.Tag = data;
                lstTimelines.Items.Add(item);
            }
            if (lstTimelines.Items.Count > 0)
                lstTimelines.Items[0].Selected = true;
        }

        private void Open()
        {
            if (lstTimelines.Items.Count > 0 && lstTimelines.SelectedItems.Count == 1)
            {
                if (TimeLine.Line != null && !Prompt.Confirm("Открытие хроники закроет текущую без сохранения, продолжить ?", "Внимание!"))
                    return;
                TimeLine.Line = (TimeLine)lstTimelines.SelectedItems[0].Tag;
                Close();
            }
        }

        private void btnOpenTimeline_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void btnDeleteTimeline_Click(object sender, EventArgs e)
        {
            if (lstTimelines.Items.Count > 0 && lstTimelines.SelectedItems.Count == 1 && Prompt.Confirm("Удалить хронику ?", "Внимание !"))
            {
                TimeLine data = (TimeLine)lstTimelines.SelectedItems[0].Tag;
                if (TimeLine.Line != null && TimeLine.Line.Id == data.Id)
                    TimeLine.Line = null;
                TimeLine.Delete(data.Id);
                Requery();
            }
        }

        private void lstTimelines_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Open();
        }


    }
}
