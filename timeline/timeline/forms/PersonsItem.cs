﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Timeline
{
    public partial class PersonsItem : Form
    {

        //Персон
        private Person _Person;

        /// <summary>
        /// Конструктор
        /// </summary>
        public PersonsItem(Person Person)
        {
            InitializeComponent();
            this._Person = Person;
        }

        //Загрузка
        private void PersonsItem_Load(object sender, EventArgs e)
        {
            if (this._Person != null)
            {
                txtLastName.Text = this._Person.Name;
                txtMemo.Text = this._Person.Memo;
                txtNotes.Text = this._Person.Notes;
                txtLastName.Text = this._Person.LastName;
                txtFirstName.Text = this._Person.FirstName;
                txtMiddleName.Text = this._Person.MiddleName;
                txtType.Text = this._Person.PersonType;
                txtGender.Text = this._Person.Gender;
                if (this._Person.Birth.HasValue)
                {
                    dtpBirth.Checked = true;
                    dtpBirth.Value = this._Person.Birth.Value;
                }
                else
                    dtpBirth.Checked = false;
                if (this._Person.Death.HasValue)
                {
                    dtpDeath.Checked = true;
                    dtpDeath.Value = this._Person.Death.Value;
                }
                else
                    dtpDeath.Checked = false;
            }
        }

        //Отмена
        private void butCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Сохранить
        private void butOK_Click(object sender, EventArgs e)
        {
            //Создаём
            if (this._Person == null)
                this._Person = Person.Create(txtName.Text);

            //Сохраняем
            this._Person.Memo = txtMemo.Text;
            this._Person.Notes = txtNotes.Text;
            this._Person.LastName = txtLastName.Text;
            this._Person.FirstName = txtFirstName.Text;
            this._Person.MiddleName = txtMiddleName.Text;
            this._Person.PersonType = txtType.Text;
            this._Person.Gender = txtGender.Text;
            this._Person.Birth = dtpBirth.Checked ? (DateTime?)null : dtpBirth.Value;
            this._Person.Death = dtpDeath.Checked ? (DateTime?)null : dtpDeath.Value;
            this._Person.Update();

            //Выход
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }



    }
}
