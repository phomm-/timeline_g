﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Timeline
{
    public partial class AreasForm : Form
    {

        public Event BindEvent { get; private set; }
        public AreasForm()
        {
            InitializeComponent();
            lstAreas.CheckBoxes = true;
            lstAreas.View = View.Details;
            lstAreas.Columns.Add(Consts.FieldName, 150);
            lstAreas.Columns.Add(Consts.FieldCountry, 150);
            lstAreas.Columns.Add(Consts.FieldCity, 150);
            lstAreas.Columns.Add(Consts.FieldPlace, 150);
            lstAreas.Columns.Add(Consts.FieldInfo, 400);
        }

        public void Open(Event bindEvent)
        {
            BindEvent = bindEvent;
            Text = "Управление локациями. Галочка - привязка к " + BindEvent.Name;
            Requery();
            ShowDialog();
        }

        public void Requery()
        {
            lstAreas.Items.Clear();
            foreach (Area data in Area.GetAreas())
            {
                ListViewItem item = new ListViewItem();
                item.Checked = data.IsEventArea(BindEvent.Id);
                item.Text = Convert.ToString(data.Name);
                item.SubItems.Add(Convert.ToString(data.Country));
                item.SubItems.Add(Convert.ToString(data.City));
                item.SubItems.Add(Convert.ToString(data.Place));
                item.SubItems.Add(Convert.ToString(data.Memo));
                item.Tag = data;
                lstAreas.Items.Add(item);
            }
            if (lstAreas.Items.Count > 0)
                lstAreas.Items[0].Selected = true;
        }


        private void btnCreateArea_Click(object sender, EventArgs e)
        {
            AreaItem form = new AreaItem(null);
            form.ShowDialog();
            Requery();
        }

        private void btnAreaImages_Click(object sender, EventArgs e)
        {
            if (lstAreas.Items.Count > 0 && lstAreas.SelectedItems.Count == 1)
            {
                Area data = (Area)lstAreas.SelectedItems[0].Tag;
                ImagesForm form = new ImagesForm();
                form.Open(2, data.Id, data.Name);
            }
        }

        private void btnDeleteArea_Click(object sender, EventArgs e)
        {
            if (lstAreas.Items.Count > 0 && lstAreas.SelectedItems.Count == 1)
            {
                Area data = (Area)lstAreas.SelectedItems[0].Tag;
                if (Prompt.Confirm("Удалить локацию " + data.Name + " ?", "Внимание !"))
                {
                    Area.Delete(data.Id);
                    Requery();
                }
            }
        }

        private void btnEditArea_Click(object sender, EventArgs e)
        {
            if (lstAreas.Items.Count > 0 && lstAreas.SelectedItems.Count == 1)
            {
                Area data = (Area)lstAreas.SelectedItems[0].Tag;
                AreaItem form = new AreaItem(data);
                form.ShowDialog();
                Requery();
            }
        }

        private void lstAreas_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            Area data = (Area)e.Item.Tag;
            data.AlterEvent(BindEvent.Id, e.Item.Checked);
        }


    }
}
