﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Timeline
{
    /// <summary>
    /// Режим работы / инструмент
    /// </summary>
    enum MouseMode
    {
        Select,
        AddHex,
        AddElps,
        AddRomb,
        AddRect,
        AddPanel,
        AddLine,
        AddLink,
        AddPerson,
        AddData,
        Move
    }

    /// <summary>
    /// Тип вставляемого блока
    /// </summary>
    public enum BlockType
    {
        Rect,
        Elps,
        Romb,
        Hex,
        Panel,
        Line,
        Timeline
    }

    static class Options
    {
        static public bool DoBlockHeightResize = false;
        static public bool RelativeTimelineSections = false;
        static public bool ShowEventLines = false;
    }

    /// <summary>
    /// Константы, настроечные данные и словари соответствий
    /// </summary>
    static class Consts
    {
        public const string Title = "АИС Хроника";
        public const string PersonName = "Имя";
        public const string FieldName = "Название";
        public const string FieldInfo = "Описание";
        public const string EventText = "Событие";

        public const string FieldFullName = "Полное имя";
        public const string FieldNotes = "Заметки";
        public const string StorylineType = "Тип сюжетной линии";

        public const string FieldCity = "Город";
        public const string FieldCountry = "Страна";
        public const string FieldPlace = "Адрес/место";
        public const string FieldDateStart = "Дата начала";
        public const string FieldDateEnd = "Дата конца";

        public const string LineText = "Сюжетная линия";
        public const string Tune = "Настройка";
        public const string TuneEvent = Tune + " события";
        public const string TuneArea = Tune + " локации";
        public const string TunePerson = Tune + " персонажа";
        public const string TuneTimeline = Tune + " хроники";
        public const string TuneImages = Tune + " изображений";
        public const string TuneStoryLines = Tune + " сюжетных линий";
        public const string FitEventToDate = "Совмещение события по дате";

        public const string DATA_FONT_NAME = "Courier";
        public const int DATA_FONT_SIZE = 10;
        public const int DATA_CHAR_WIDTH = 10;
        public const int DATA_CHAR_HEIGHT = 16;

        static public Size BlockSize = new Size(150, 50);
        public const int TimelineHeight = 50;
        public const int MinTimelineWidth = 250;
        static public Rectangle LineTextZone = new Rectangle(5, 5, 110, 90);        
        static public Pen pen = new Pen(Color.Black) { DashStyle = DashStyle.Dash, Width = 1, DashOffset = 0.5f };

        public static Dictionary<MouseMode, BlockType> ModeToBlock = new Dictionary<MouseMode, BlockType>()
        {
            {MouseMode.AddElps, BlockType.Elps}, {MouseMode.AddRomb, BlockType.Romb}, {MouseMode.AddRect, BlockType.Rect}, 
            {MouseMode.AddHex, BlockType.Hex}, {MouseMode.AddPanel, BlockType.Panel}, {MouseMode.AddLine, BlockType.Line}
        };

        public static Dictionary<string, string> ToolTips = new Dictionary<string, string>()
        {
            {"Select", "Выбор"}, {"AddHex", "Событие"}, {"AddElps", "Событие"}, {"AddRomb", "Событие"}, 
            {"AddRect", "Событие"}, {"AddPanel", "Группа"}, {"AddLine", "Сюжетная линия"}, {"AddLink", "Связь"}, 
            {"AddPerson", "Персонаж"}, {"Move", "Позиция/Размер"}, {"Color", "Цвет"}, {"PenWidth", "Толщина"}, 
            {"DelBlock", "Удаление"}, {"DelLink", "Удаление связи"}, {"Clear", "Очистка схемы"}, 
            {"AddData", "Текст блока"}
        };

        public static Dictionary<MouseMode, Cursor> ModeCursors = new Dictionary<MouseMode, Cursor>()
        {
            {MouseMode.Select, Cursors.Hand}, {MouseMode.AddElps, Cursors.Arrow}, {MouseMode.AddHex, Cursors.Arrow}, 
            {MouseMode.AddRomb, Cursors.Arrow}, {MouseMode.AddRect, Cursors.Arrow}, {MouseMode.AddPanel, Cursors.Arrow}, 
            {MouseMode.AddLine, Cursors.Arrow}, {MouseMode.AddLink, Cursors.SizeNWSE}, {MouseMode.Move, Cursors.SizeAll},
            {MouseMode.AddData, Cursors.Arrow}, {MouseMode.AddPerson, Cursors.Arrow}
        };
    }
}
