﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MyData data = new MyData(new string[] { "sex", "age", "language", "os", "country", "continent" });
            dataGridView1.DataSource = data.Data();
            label1.Text = string.Format("колонок: {0} строк: {1}", data.colCount, data.rowCount);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int[] myAr = textBox1.Text.Split(new char[]{' ', ','}).Select(s => { int n; return int.TryParse(s, out n)? n: 0;}).ToArray();
            MessageBox.Show(string.Join(",", myAr));
        }
    }

    public class MyData
    {
        private DataTable dt;
        private string[] fields;

        public int rowCount
        {
            get
            {
                return dt.Rows.Count;
            }
        }

        public int colCount
        {
            get
            {
                return dt.Columns.Count;
            }
        }

        public MyData(string[] fields)
        {
            dt = new DataTable();
            this.fields = fields;
        }

        public DataTable Data()
        {
            MySqlConnectionStringBuilder mysqlCSB = new MySqlConnectionStringBuilder();

            //mysqlCSB.Server = "meinkompf";
            //mysqlCSB.Database = "ololo";
            //mysqlCSB.UserID = "root";
            //mysqlCSB.Password = "ph";
            string queryString = @"SELECT " + string.Join(",", fields) + " FROM programmers";

            using (MySqlConnection con = new MySqlConnection())
            {
                con.ConnectionString = "server=127.0.0.1;uid=phomm;pwd=ph;database=ololo;"; //*/mysqlCSB.ConnectionString;

                MySqlCommand com = new MySqlCommand(queryString, con);

                try
                {
                    con.Open();

                    using (MySqlDataReader dr = com.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            dt.Load(dr);
                        }
                    }
                }

                catch (MySql.Data.MySqlClient.MySqlException ex)
                {
                    switch (ex.Number)
                    {
                        case 0:
                            MessageBox.Show("Cannot connect to server.  Contact administrator");
                            break;
                        case 1045:
                            MessageBox.Show("Invalid username/password, please try again");
                            break;
                        default:
                            MessageBox.Show(string.Format("{0} Code: {1}", ex.Message, ex.Number));
                            break;
                    }
                }
            }
            return dt;
        }
    }
}

