CREATE DATABASE ololo;

GRANT ALL PRIVILEGES ON ololo.* TO phomm@localhost IDENTIFIED BY 'ph';

CREATE TABLE programmers (
id int(11) NOT NULL auto_increment,
sex char(1) default NULL,
age int(11) default NULL,
language varchar(30) default NULL,
os varchar(30) default NULL,
country varchar(30) default NULL,
continent varchar(30) default NULL,
PRIMARY KEY (id)
);

INSERT INTO programmers (sex, age, language, os, country, continent) VALUES ('F', 33, 'PHP', 'Linux', 'USA', 'North America'), 
('M', 41, 'Java', 'Solaris', 'USA', 'North America'),
('M', 31, 'C++', 'Solaris', 'USA', 'North America'),
('M', 45, 'Lisp', 'MacOS', 'USA', 'North America'),
('M', 25, 'C', 'Solaris', 'Antarctica', 'Antarctica'),
('F', 17, 'PHP', 'Linux', 'Denmark', 'Europe'),
('M', 21, 'Perl', 'Linux', 'UK', 'Europe'),
('M', 14, 'PHP', 'Linux', 'UK', 'Europe'),
('F', 21, 'Perl', 'Linux', 'Germany', 'Europe'),
('F', 38, 'PHP', 'Linux', 'Germany', 'Europe'),
('M', 26, 'C++', 'Windows', 'USA', 'North America'),
('M', 22, 'PHP', 'Windows', 'France', 'Europe'),
('M', 17, 'PHP', 'Linux', 'Japan', 'Asia'),
('F', 38, 'C', 'Solaris', 'South Korea', 'Asia'),
('F', 19, 'PHP', 'Linux', 'Canada', 'North America'),
('F', 32, 'Perl', 'Linux', 'France', 'Europe'),
('M', 32, 'Java', 'Solaris', 'Mexico', 'North America'),
('F', 23, 'PHP', 'Solaris', 'Brazil', 'South America'),
('F', 19, 'PHP', 'Linux', 'Finland', 'Europe'),
('M', 21, 'PHP', 'Solaris', 'Brazil', 'South America'),
('M', 51, 'Java', 'Linux', 'UK', 'Europe'),
('M', 29, 'Java', 'Linux', 'Japan', 'Asia'),
('M', 29, 'Java', 'Solaris', 'China', 'Asia'),
('M', 21, 'C++', 'MacOS', 'Germany', 'Europe'),
('M', 21, 'Perl', 'Solaris', 'France', 'Europe'),
('M', 27, 'PHP', 'Linux', 'India', 'Asia'),
('M', 31, 'Perl', 'Linux', 'India', 'Asia'),
('M', 17, 'C', 'Linux', 'Pakistan', 'Asia'),
('M', 45, 'PHP', 'Windows', 'USA', 'North America'),
('F', 22, 'Java', 'Windows', 'Italy', 'Europe'),
('F', 33, 'C', 'Linux', 'Spain', 'Europe');
